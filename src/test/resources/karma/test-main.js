var tests = [];

for (var file in window.__karma__.files) {
    if (window.__karma__.files.hasOwnProperty(file)) {
        if (/-test\.js$/.test(file)) {
            tests.push(file);
        }
    }
}

requirejs.config({
    // Karma serves files from '/base'
    baseUrl: '/base/src',

    paths: {
        // dependencies
        'jquery': '../target/qunit/dependencies/js/external/jquery/jquery',
        'aui-atlassian': '../target/qunit/dependencies/js/atlassian/atlassian',
        'aui-event': '../target/qunit/dependencies/js/atlassian/event',
        'aui-dialog': '../target/qunit/dependencies/js/atlassian/dialog',
        'credentials-dialog': '../target/qunit/soy/credentials-dialog.soy',
        'soy-utils': '../target/qunit/soyutils/js/soyutils',
        'edit-task': 'main/resources/js/editTask'
    },

    shim: {
        'edit-task': {
            deps: [
                'credentials-dialog'
            ],
            exports: 'confDeploy'
        },
        'aui-atlassian': {
            deps: [
                'jquery'
            ]
        },
        'aui-event': {
            deps: [
                'aui-atlassian'
            ]
        },
        'aui-dialog': {
            deps: [
                'aui-atlassian',
                'aui-event'
            ]
        },
        'credentials-dialog': {
            deps: [
                'aui-dialog',
                'soy-utils'
            ],
            exports: 'confdeploy'
        }
    },

    // ask Require.js to load these files (all our tests)
    deps: tests,

    // start test run, once Require.js is done
    callback: window.__karma__.start
});