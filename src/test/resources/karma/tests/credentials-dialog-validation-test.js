/**
 * Test module that exercises the client-side validation of the edit credentials dialog
 */
define(['edit-task'], function(confDeploy) {

    module("Test Credentials Dialog - validation tests", {
        setup: function() {
            // Render the HTML fixture
            AJS.$("body").append(window.__html__['src/test/resources/karma/fixtures/blank-form.html']);
        },
        teardown: function() {
            AJS.$("body").empty();
        }
    });

    test("Product login - username and password required", function() {

        // Click the 'Edit Credentials' button to load the dialog.
        AJS.$("#bcpd_config_credentials").click();

        // Use product login
        var useProductLogin = AJS.$("#bcpd-login-useAtlassianId-productLogin");
        equal(useProductLogin.length, 1);
        useProductLogin.click();

        // Try to save the form - should get validation errors.
        var saveButton = AJS.$(".button-panel-submit-button");
        equal(saveButton.length, 1);
        saveButton.click();

        // Dialog should have remained open
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 1);

        // Ensure 'username is required' error was raised
        var usernameField = AJS.$("#bcpd-login-username");
        var usernameError = AJS.$("div.error", usernameField.parent());
        equal(usernameError.length, 1);
        equal(usernameError.text(), "bcpd.config.dialog.login.username.error.required");

        // Focusing on the username field should clear the error
        usernameField.focus();
        usernameError = AJS.$("div.error", usernameField.parent());
        equal(usernameError.length, 0);

        // Ensure 'password is required' error was raised
        var passwordField = AJS.$("#bcpd-login-password");
        var passwordError = AJS.$("div.error", passwordField.parent());
        equal(passwordError.length, 1);
        equal(passwordError.text(), "bcpd.config.dialog.login.password.error.required");

        // Focusing on the password field should clear the error
        passwordField.focus();
        passwordError = AJS.$("div.error", passwordField.parent());
        equal(passwordError.length, 0);
    });

    test("Product login - password variable must be a bamboo variable", function() {

        // Click the 'Edit Credentials' button to load the dialog.
        AJS.$("#bcpd_config_credentials").click();

        // Use product login
        var useProductLogin = AJS.$("#bcpd-login-useAtlassianId-productLogin");
        equal(useProductLogin.length, 1);
        useProductLogin.click();

        // Select 'use password variable'.
        var usePasswordVariable = AJS.$("#bcpd-login-usePasswordVariable");
        usePasswordVariable.click();

        // Type something that's not a bamboo variable into the field
        var passwordVariableField = AJS.$("#bcpd-login-passwordVariable");
        passwordVariableField.val("password");

        // Try to save the form - should get validation errors.
        var saveButton = AJS.$(".button-panel-submit-button");
        equal(saveButton.length, 1);
        saveButton.click();

        // Dialog should have remained open
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 1);

        // 'password variable must be a bamboo variable' error should have been raised
        var passwordVariableError = AJS.$("div.error", passwordVariableField.parent());
        equal(passwordVariableError.length, 1);
        equal(passwordVariableError.text(), "bcpd.config.dialog.login.password.error.format");

        // Focusing on the field should clear the error
        passwordVariableField.focus();
        passwordVariableError = AJS.$("div.error", passwordVariableField.parent());
        equal(passwordVariableError.length, 0);
    });

    test("Atlassian ID WebSudo - appname is required", function() {

        // load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Select Atlassian ID Login
        var useAtlassianId = AJS.$("#bcpd-login-useAtlassianId-atlassianId");
        useAtlassianId.click();

        // Enter a username and password
        AJS.$("#bcpd-login-username").val("user@atlassian.com");
        AJS.$("#bcpd-login-password").val("password");

        // Activate the WebSudo panel
        AJS.$("li.page-menu-item button:contains(\"WebSudo\")", "div.aui-dialog").click();

        // Select Atlassian ID WebSudo
        var useAtlassianIdWebSudo = AJS.$("#bcpd-websudo-method-lasso");
        useAtlassianIdWebSudo.click();

        // Try to save the form - should get validation errors.
        var saveButton = AJS.$(".button-panel-submit-button");
        equal(saveButton.length, 1);
        saveButton.click();

        // Dialog should have remained open
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 1);

        // 'AppName is required' error should have been raised.
        var webSudoAppNameField = AJS.$("#bcpd-websudo-appname");
        var webSudoAppNameError = AJS.$("div.error", webSudoAppNameField.parent());
        equal(webSudoAppNameError.length, 1);
        equal(webSudoAppNameError.text(), "bcpd.config.dialog.websudo.appName.error.required");

        // Focusing in the field should clear the error.
        webSudoAppNameField.focus();
        webSudoAppNameError = AJS.$("div.error", webSudoAppNameField.parent());
        equal(webSudoAppNameError.length, 0);
    });

    test("Product WebSudo - websudo password is required", function() {

        // load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Select Atlassian ID Login
        var useAtlassianId = AJS.$("#bcpd-login-useAtlassianId-atlassianId");
        useAtlassianId.click();

        // Enter a username and password
        AJS.$("#bcpd-login-username").val("user@atlassian.com");
        AJS.$("#bcpd-login-password").val("password");

        // Activate the WebSudo panel
        AJS.$("li.page-menu-item button:contains(\"WebSudo\")", "div.aui-dialog").click();

        // Select Product-based WebSudo
        var useProductWebSudo = AJS.$("#bcpd-websudo-method-product");
        equal(useProductWebSudo.length, 1);
        useProductWebSudo.click();

        // Try to save the form - should get validation errors.
        var saveButton = AJS.$(".button-panel-submit-button");
        equal(saveButton.length, 1);
        saveButton.click();

        // Dialog should have remained open
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 1);

        // 'WebSudo password is required' error should have been raised.
        var webSudoPasswordField = AJS.$("#bcpd-websudo-password");
        equal(webSudoPasswordField.length, 1);
        var webSudoPasswordError = AJS.$("div.error", webSudoPasswordField.parent());
        equal(webSudoPasswordError.length, 1);
        equal(webSudoPasswordError.text(), "bcpd.config.dialog.login.password.error.required");

        // Focusing in the field should clear the error.
        webSudoPasswordField.focus();
        webSudoPasswordError = AJS.$("div.error", webSudoPasswordField.parent());
        equal(webSudoPasswordError.length, 0);
    });
});
