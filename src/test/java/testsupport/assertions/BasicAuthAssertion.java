package testsupport.assertions;

import org.apache.http.Header;
import org.apache.http.client.methods.HttpUriRequest;
import org.junit.Assert;

import java.util.Base64;

/**
 * Asserts that a {@link HttpUriRequest} has the expected basic auth header.
 */
public class BasicAuthAssertion implements HttpUriRequestAssertion
{
    private final String expectedUsername;
    private final String expectedPassword;

    public BasicAuthAssertion(final String expectedUsername, final String expectedPassword)
    {
        this.expectedPassword = expectedPassword;
        this.expectedUsername = expectedUsername;
    }

    @Override
    public void assertRequest(HttpUriRequest request)
    {
        String passwordString = expectedUsername + ":" + expectedPassword;
        String basicAuthHeader = Base64.getEncoder().encodeToString(passwordString.getBytes());
        Header[] authorizations = request.getHeaders("Authorization");
        Assert.assertEquals("Expect only 1 Authorization header", 1, authorizations.length);
        Assert.assertEquals("Authorization", authorizations[0].getName());
        Assert.assertEquals("Basic "+ basicAuthHeader, authorizations[0].getValue());
    }
}
