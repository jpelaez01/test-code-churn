package testsupport.assertions;

import org.apache.http.client.methods.HttpUriRequest;

import static org.junit.Assert.assertEquals;

/**
 *
 */
public class MethodTypeAssertion implements HttpUriRequestAssertion
{
    @Override
    public void assertRequest(HttpUriRequest request)
    {
        assertEquals(request.getMethod(), method.name());
    }

    public enum Method {GET, POST, PUT, DELETE } // Not exhaustive, but should cover what I need for now.

    private final Method method;

    public MethodTypeAssertion(final Method method)
    {
        this.method = method;
    }
}
