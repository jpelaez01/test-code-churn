package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.fugue.Pair;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * Test harness for the {@link Crypto} class.
 */
public class TestCrypto
{
    private final Random randomizer = new Random();

    /**
     *  Ensures my crappy cryptographic tool is symmetric (ie. the encrypted value can be decrypted back to the original value)
     */
    @Test
    public void testSymmetry()
    {
        for (int i = 0; i < 5000; i++)
        {
            final String original = getRandomASCIIString(64);
            Pair<String,String> encrypted = Crypto.encrypt(original);

            final String decrypted = Crypto.decrypt(encrypted.left(), encrypted.right());

            assertEquals(original, decrypted);
        }
    }

    private String getRandomASCIIString(int length)
    {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++)
        {
            // ASCII printable chars exist between 20 and 126 inclusive
            int charCode = (int)(Math.ceil((randomizer.nextDouble() * 106) + 20));
            char asciiChar = (char)charCode;
            builder.append(asciiChar);
        }
        return builder.toString();
    }
}
