package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.bamboo.plan.artifact.ArtifactContext;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionContext;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionContext;
import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.fugue.Either;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link JobArtifactRetriever}
 */
public class TestJobArtifactRetriever
{
    private static final String TEST_ARTIFACT_NAME = "My Bamboo Artifact";
    private static final Long TEST_ARTIFACT_ID = 123456L;

    private ArtifactRetriever jobArtifactRetriever;
    private File testArtifactFile;

    @Mock
    private TaskContext mockTaskContext;
    @Mock
    private BuildContext mockBuildContext;
    @Mock
    private ArtifactContext mockArtifactContext;

    @Mock
    private CustomVariableContext mockVariableContext;

    @Before
    public void setUp() throws IOException
    {
        MockitoAnnotations.initMocks(this);

        when(mockTaskContext.getBuildContext()).thenReturn(mockBuildContext);
        when(mockBuildContext.getArtifactContext()).thenReturn(mockArtifactContext);

        jobArtifactRetriever = new JobArtifactRetriever(mockTaskContext, mockVariableContext);

        testArtifactFile = File.createTempFile("abc", "123");
    }

    @After
    public void tearDown()
    {
        testArtifactFile.delete();
    }

    /**
     * Tests retrieving an artifact file when the configuration just serialized the artifact's name
     */
    @Test
    public void testGetFileByArtifactName()
    {
        mockArtifact(TEST_ARTIFACT_ID, TEST_ARTIFACT_NAME, testArtifactFile);

        Either<File,Failure> maybeFile = jobArtifactRetriever.getFileFromArtifact(TEST_ARTIFACT_NAME);

        assertTrue(maybeFile.isLeft());
        assertEquals(testArtifactFile, maybeFile.left().get());
    }

    /**
     * Ensures a failure when retrieving an artifact file by it's name, where there is no matching artifact subscription
     */
    @Test
    public void testGetFileByArtifactName_ArtifactMissing()
    {
        mockArtifact(TEST_ARTIFACT_ID, "NOT THE RIGHT NAME", null);

        Either<File,Failure> maybeFile = jobArtifactRetriever.getFileFromArtifact(TEST_ARTIFACT_NAME);
        assertTrue(maybeFile.isRight());
        assertEquals("Unable to locate Artifact subscription named " + TEST_ARTIFACT_NAME, maybeFile.right().get().getMessage());
    }

    /**
     * Tests retrieving a file artifact by its v2 serialization string.
     */
    @Test
    public void testGetFile()
    {
        final String artifactString = StringUtils.join(new Object[] {"v2", TEST_ARTIFACT_ID, -1L, -1L, TEST_ARTIFACT_NAME}, ":");
        mockArtifact(TEST_ARTIFACT_ID, TEST_ARTIFACT_NAME, testArtifactFile);

        Either<File, Failure> maybeFile = jobArtifactRetriever.getFileFromArtifact(artifactString);

        assertTrue(maybeFile.isLeft());
        assertEquals(testArtifactFile, maybeFile.left().get());
    }

    /**
     * Ensures a failure when trying to retrieve an artifact file from an artifact subscription that doesn't exist.
     */
    @Test
    public void testGetFile_ArtifactMissing()
    {
        final String artifactString = StringUtils.join(new Object[] {"v2", TEST_ARTIFACT_ID, -1L, -1L, TEST_ARTIFACT_NAME}, ":");
        mockArtifact(99999L, TEST_ARTIFACT_NAME, testArtifactFile);

        Either<File, Failure> maybeFile = jobArtifactRetriever.getFileFromArtifact(artifactString);
        assertTrue(maybeFile.isRight());

        final String expectedError =  String.format("Unable to find the artifact with id %d in this task's context. " +
            "Has an artifact subscription been configured for that artifact in this job?", TEST_ARTIFACT_ID);
        assertEquals(expectedError, maybeFile.right().get().getMessage());
    }

    /**
     * Ensures a failure when trying to retrieve an artifact file from an artifact subscription that exists, but for some
     * reason the file on disk that it represents is missing or inaccessible (eg. the artifact transfer failed silently)
     */
    @Test
    public void testGetFile_FileMissing()
    {
        final String artifactString = StringUtils.join(new Object[] {"v2", TEST_ARTIFACT_ID, -1L, -1L, TEST_ARTIFACT_NAME}, ":");
        File dummyFile = new File("/doesnot/exist");
        mockArtifact(TEST_ARTIFACT_ID, TEST_ARTIFACT_NAME, dummyFile);

        Either<File, Failure> maybeFile = jobArtifactRetriever.getFileFromArtifact(artifactString);
        assertTrue(maybeFile.isRight());
        final String expectedError = String.format("Artifact file %s is either missing or not a file.", dummyFile.getAbsolutePath());;
        assertEquals(expectedError, maybeFile.right().get().getMessage());
    }

    /**
     * Mocking boilerplate.
     */
    private void mockArtifact(final Long artifactId, final String artifactName, final File artifact)
    {
        ArtifactDefinitionContext definition = mock(ArtifactDefinitionContext.class);
        when(definition.getId()).thenReturn(artifactId);
        when(definition.getName()).thenReturn(artifactName);

        ArtifactSubscriptionContext subscription = mock(ArtifactSubscriptionContext.class);
        when(subscription.getArtifactDefinitionContext()).thenReturn(definition);
        if (artifact != null)
        {
            when(subscription.getEffectiveDestinationPath()).thenReturn(artifact.getAbsolutePath());
        }

        when(mockArtifactContext.getSubscriptionContexts()).thenReturn(Collections.singletonList(subscription));
    }
}
