package com.atlassian.bamboo.plugins.confdeploy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test for {@link AvailableArtifact}
 */
public class TestAvailableArtifact {

    @Test
    public void testValidate() {
        assertEquals(false, AvailableArtifact.isValidStoredArtifact(""));
        assertEquals(false, AvailableArtifact.isValidStoredArtifact("My Plugin - OBR"));
        assertEquals(false, AvailableArtifact.isValidStoredArtifact("v2:1::0:Missing task id"));
        assertEquals(false, AvailableArtifact.isValidStoredArtifact("1:2:3:0:Missing v2"));

        assertEquals(true, AvailableArtifact.isValidStoredArtifact("v2:3768405:2:0:My Plugin - OBR"));
    }
}
