package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.lasso;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.fugue.Either;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import testsupport.MockTaskConfigurationFactory;
import testsupport.assertions.*;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static testsupport.MockTaskConfigurationFactory.*;

/**
 * Tests {@link AtlassianIdLoginRequestBehaviour}.
 */
public class TestAtlassianIdLoginRequestBehaviour
{
    private static final String TEST_BASE_URL = "https://id.atlassian.com";
    private static final String TEST_USERNAME = "fred";
    private static final String TEST_PASSWORD = "passwordz";

    private AtlassianIdLoginRequestBehaviour requestBehaviour;
    private TaskConfiguration mockConfig;

    @Before
    public void setUp()
    {
        requestBehaviour = new AtlassianIdLoginRequestBehaviour();
    }

    /**
     * Ensures that request construction fails if there is no username specified.
     */
    @Test(expected = IllegalArgumentException.class)
    public void ensureAtlassianIdUsernameIsRequired()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                ATLASSIAN_ID_BASE_URL, TEST_BASE_URL,
                ATLASSIAN_ID_USERNAME, "",
                ATLASSIAN_ID_PASSWORD, TEST_PASSWORD
        ));

        requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap()); // should throw
    }

    /**
     * Ensures that request construction fails if there is no password specified.
     */
    @Test(expected = IllegalArgumentException.class)
    public void ensureAtlassianIdPasswordIsRequired()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                ATLASSIAN_ID_BASE_URL, TEST_BASE_URL,
                ATLASSIAN_ID_USERNAME, TEST_USERNAME,
                ATLASSIAN_ID_PASSWORD, ""
        ));

        requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap()); // should throw
    }

    /**
     * Ensures that the request construction fails if there is no Base URL specified.
     */
    @Test(expected = IllegalArgumentException.class)
    public void ensureAtlassianIdBaseUrlIsRequired()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                ATLASSIAN_ID_BASE_URL, "",
                ATLASSIAN_ID_USERNAME, TEST_USERNAME,
                ATLASSIAN_ID_PASSWORD, TEST_PASSWORD
        ));

        requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap()); // should throw
    }

    /**
     * Ensures the request is constructed with the correct URL and request body.
     */
    @Test
    public void ensureRequestConstruction() throws IOException, JSONException
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                ATLASSIAN_ID_BASE_URL, TEST_BASE_URL,
                ATLASSIAN_ID_USERNAME, TEST_USERNAME,
                ATLASSIAN_ID_PASSWORD, TEST_PASSWORD
        ));

        Either<HttpUriRequest, Failure> maybeRequest = requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertFalse(maybeRequest.isRight());
        assertTrue(maybeRequest.isLeft());

        HttpUriRequest request = maybeRequest.left().get();
        HttpUriRequestAssertionHelper.assertRequest(request, ImmutableList.<HttpUriRequestAssertion>of(
                new MethodTypeAssertion(MethodTypeAssertion.Method.POST),
                new RequestUrlAssertion(TEST_BASE_URL + "/id/rest/login"),
                new JsonRequestBodyAssertion(new Predicate<JSONObject>()
                {

                    @Override
                    public boolean apply(JSONObject body)
                    {
                        try
                        {
                            assertEquals(TEST_USERNAME, body.get("username"));
                            assertEquals(TEST_PASSWORD, body.get("password"));
                        }
                        catch (JSONException e)
                        {
                            fail(e.toString());
                        }
                        return true;
                    }
                })
        ));
    }

    /**
     * Ensures the response handler detects the success condition successfully.
     */
    @Test
    public void testResponseSuccess() throws IOException
    {
        StatusLine mockStatus = Mockito.mock(StatusLine.class);
        HttpResponse mockResponse = Mockito.mock(HttpResponse.class);

        when(mockResponse.getStatusLine()).thenReturn(mockStatus);
        when(mockStatus.getStatusCode()).thenReturn(200);

        Result result = requestBehaviour.handleResponse(mockResponse);
        assertTrue(result.isSuccess());
    }

    /**
     * Ensures the response handler detects a failure condition successfully.
     */
    @Test
    public void testResponseFailure() throws IOException
    {
        StatusLine mockStatus = Mockito.mock(StatusLine.class);
        HttpResponse mockResponse = Mockito.mock(HttpResponse.class);

        when(mockResponse.getStatusLine()).thenReturn(mockStatus);
        when(mockStatus.getStatusCode()).thenReturn(500);

        Result result = requestBehaviour.handleResponse(mockResponse);
        assertFalse(result.isSuccess());
    }
}
