package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.fugue.Either;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link DeploymentArtifactRetriever}
 */
public class TestDeploymentArtifactRetriever
{
    private static final String TEST_ARTIFACT_NAME = "Plugin Build";
    private static final long TEST_ARTIFACT_ID = 184935L;
    private static final long TEST_TRANSFER_TASK_ID = 99834L;
    private static final int TEST_TRANSFER_ID = 1;

    private ArtifactRetriever deploymentArtifactRetriever;
    private File testArtifact;

    @Mock
    private DeploymentTaskContext deploymentTaskContext;
    @Mock
    private CustomVariableContext customVariableContext;

    @Before
    public void setUp() throws IOException
    {
        MockitoAnnotations.initMocks(this);

        deploymentArtifactRetriever = new DeploymentArtifactRetriever(deploymentTaskContext, customVariableContext);

        testArtifact = File.createTempFile("abc", "123");
    }

    @After
    public void tearDown()
    {
        testArtifact.delete();
    }

    /**
     * Ensures that the DeploymentArtifactRetriever refuses to locate artifacts that were not downloaded by a transfer
     * task (these should only be handled by {@link JobArtifactRetriever}
     */
    @Test
    public void testNonTransferTaskArtifactFails()
    {
        final String artifactString = "v2:123456:-1:-1:My Artifact Name";

        Either<File,Failure> maybeFile = deploymentArtifactRetriever.getFileFromArtifact(artifactString);
        assertTrue(maybeFile.isRight());
        assertEquals("Configured plugin artifact was not acquired by a transfer task, but this is a deployment project. WTF?", maybeFile.right().get().getMessage());
    }
}
