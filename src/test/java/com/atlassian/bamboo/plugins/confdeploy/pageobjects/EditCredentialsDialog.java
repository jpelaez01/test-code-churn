package com.atlassian.bamboo.plugins.confdeploy.pageobjects;

import com.atlassian.bamboo.plugins.confdeploy.config.ui.UiFields;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Map;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.1
 */
public class EditCredentialsDialog
{
    @FindBy(id = "bcpd-login-username")
    private WebElement username;

    @FindBy(id = "bcpd-login-password")
    private WebElement password;

    @FindBy(className = "button-panel-submit-button") // TODO: this is probably not specific enough to be robust
    private WebElement save;

    public void updateCredentials(Map<String, String> config)
    {
        // TODO: Don't use this method. Have individual setters and getters, maybe?

        if (config.containsKey(UiFields.USERNAME))
        {
            username.sendKeys(config.get(UiFields.USERNAME));
        }

        if (config.containsKey(UiFields.PASSWORD))
        {
            password.sendKeys(config.get(UiFields.PASSWORD));
        }

        // TODO: This may fail if there are validation errors present.
        save.click();
    }
}
