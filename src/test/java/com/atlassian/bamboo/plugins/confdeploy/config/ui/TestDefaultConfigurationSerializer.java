package com.atlassian.bamboo.plugins.confdeploy.config.ui;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask;
import com.atlassian.bamboo.plugins.confdeploy.Crypto;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.webwork.util.ActionParametersMapImpl;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Test harness for {@link DefaultConfigurationSerializer}
 */
public class TestDefaultConfigurationSerializer
{
    private ConfigurationSerializer serializer;

    @Before
    public void setUp()
    {
        serializer = new DefaultConfigurationSerializer();
    }

    @Test
    public void testSerializeProductLogin()
    {
        ActionParametersMap params = new ActionParametersMapImpl(new HashMap<String, Object>());
        params.put(UiFields.USERNAME, "admin");
        params.put(UiFields.PASSWORD, "password");
        params.put(UiFields.BASE_URL, "http://localhost:1990/confluence");
        params.put(UiFields.USE_ATLASSIAN_ID, false);
        params.put(UiFields.PLUGIN_ARTIFACT, "pluginArtifact");

        TaskDefinition task = Mockito.mock(TaskDefinition.class);

        Map<String, String> serializedParams = serializer.serializeTaskContext(new TaskParametersMap(params), task);


        assertEquals("admin", serializedParams.get(ConfigFields.USERNAME));
        assertTrue(serializedParams.containsKey(ConfigFields.PASSWORD_ENCRYPTION_KEY));
        assertTrue(serializedParams.containsKey(ConfigFields.PASSWORD));
        assertEquals("password", Crypto.decrypt(serializedParams.get(ConfigFields.PASSWORD_ENCRYPTION_KEY), serializedParams.get(ConfigFields.PASSWORD)));
        assertEquals("pluginArtifact", serializedParams.get(ConfigFields.PLUGIN_ARTIFACT));
        assertEquals("http://localhost:1990/confluence", serializedParams.get(ConfigFields.BASE_URL));
    }

    @Test
    public void testSerializeProductLoginWithPasswordVariable()
    {
        ActionParametersMap params = new ActionParametersMapImpl(new HashMap<String, Object>());
        params.put(UiFields.USERNAME, "admin");
        params.put(UiFields.USE_PASSWORD_VARIABLE, true);
        params.put(UiFields.PASSWORD_VARIABLE, "${bamboo.mypassword}");
        params.put(UiFields.BASE_URL, "http://localhost:1990/confluence");
        params.put(UiFields.USE_ATLASSIAN_ID, false);
        params.put(UiFields.PLUGIN_ARTIFACT, "pluginArtifact");

        TaskDefinition task = Mockito.mock(TaskDefinition.class);

        Map<String, String> serializedParams = serializer.serializeTaskContext(new TaskParametersMap(params), task);


        assertEquals("admin", serializedParams.get(ConfigFields.USERNAME));
        assertTrue(StringUtils.isBlank(serializedParams.get(ConfigFields.PASSWORD_ENCRYPTION_KEY)));
        assertTrue(StringUtils.isBlank(serializedParams.get(ConfigFields.PASSWORD)));
        assertEquals("${bamboo.mypassword}", serializedParams.get(ConfigFields.PASSWORD_VARIABLE));

        assertFalse(Boolean.valueOf(serializedParams.get(ConfigFields.USE_ATLASSIAN_ID)));
        assertEquals("pluginArtifact", serializedParams.get(ConfigFields.PLUGIN_ARTIFACT));
        assertEquals("http://localhost:1990/confluence", serializedParams.get(ConfigFields.BASE_URL));
    }

    @Test
    public void testSerializeAtlassianIdLoginWithAtlassianIdWebSudo()
    {
        ActionParametersMap params = new ActionParametersMapImpl(new HashMap<String, Object>());
        params.put(UiFields.USE_ATLASSIAN_ID, true);
        params.put(UiFields.ATLASSIAN_ID_USERNAME, "admin@mydomain.com");
        params.put(UiFields.ATLASSIAN_ID_PASSWORD, "nimda");
        params.put(UiFields.USE_ATLASSIAN_ID_PASSWORD_VARIABLE, false);
        params.put(UiFields.USE_ATLASSIAN_ID_WEBSUDO, true);
        params.put(UiFields.ATLASSIAN_ID_APP_NAME, "whack");
        params.put(UiFields.BASE_URL, "http://localhost:1990/confluence");
        params.put(UiFields.PLUGIN_ARTIFACT, "pluginArtifact");

        TaskDefinition task = Mockito.mock(TaskDefinition.class);

        Map<String, String> serializedParams = serializer.serializeTaskContext(new TaskParametersMap(params), task);

        assertEquals("true", serializedParams.get(ConfigFields.USE_ATLASSIAN_ID));
        assertEquals("admin@mydomain.com", serializedParams.get(ConfigFields.ATLASSIAN_ID_USERNAME));
        assertTrue(serializedParams.containsKey(ConfigFields.ATLASSIAN_ID_PASSWORD_ENCRYPTION_KEY));
        assertTrue(serializedParams.containsKey(ConfigFields.ATLASSIAN_ID_PASSWORD));
        assertEquals("nimda", Crypto.decrypt(serializedParams.get(ConfigFields.ATLASSIAN_ID_PASSWORD_ENCRYPTION_KEY), serializedParams.get(ConfigFields.ATLASSIAN_ID_PASSWORD)));
        assertEquals("whack", serializedParams.get(ConfigFields.ATLASSIAN_ID_APP_NAME));
        assertTrue(StringUtils.isBlank(serializedParams.get(ConfigFields.USERNAME)));
        assertTrue(StringUtils.isBlank(serializedParams.get(ConfigFields.PASSWORD)));
    }

    @Test
    public void testSerializeProductLoginWithExistingPassword()
    {
        ActionParametersMap params = new ActionParametersMapImpl(new HashMap<String, Object>());
        params.put(UiFields.USERNAME, "admin");
        params.put(UiFields.PASSWORD, DefaultConfigurationSerializer.SAVED_PASSWORD_IDENTIFIER);
        params.put(UiFields.BASE_URL, "http://localhost:1990/confluence");
        params.put(UiFields.USE_ATLASSIAN_ID, false);
        params.put(UiFields.PLUGIN_ARTIFACT, "pluginArtifact");

        Map<String, String> existingConfig = new HashMap<String, String>();
        TaskDefinition task = Mockito.mock(TaskDefinition.class);
        when(task.getConfiguration()).thenReturn(existingConfig);
        existingConfig.put(ConfigFields.PASSWORD, "zzzzzencryptedpasswordzzzzzz");
        existingConfig.put(ConfigFields.PASSWORD_ENCRYPTION_KEY, "the-encryption-key");

        Map<String, String> serializedParams = serializer.serializeTaskContext(new TaskParametersMap(params), task);


        assertEquals("admin", serializedParams.get(ConfigFields.USERNAME));
        assertEquals("zzzzzencryptedpasswordzzzzzz", serializedParams.get(ConfigFields.PASSWORD));
        assertEquals("the-encryption-key", serializedParams.get(ConfigFields.PASSWORD_ENCRYPTION_KEY));
        assertEquals("pluginArtifact", serializedParams.get(ConfigFields.PLUGIN_ARTIFACT));
        assertEquals("http://localhost:1990/confluence", serializedParams.get(ConfigFields.BASE_URL));
    }

    @Test
    public void testDeserializeTaskContext()
    {
        Map<String, Object> destination = new HashMap<String, Object>();
        Map<String, String> source = new HashMap<String, String>();

        TaskDefinition task = mock(TaskDefinition.class);
        when(task.getConfiguration()).thenReturn(source);

        source.put(ConfigFields.USERNAME, "admin");
        source.put(ConfigFields.PASSWORD, "adkjaldkjadlkjadlkajdlakjdlaskjs");
        source.put(ConfigFields.PASSWORD_ENCRYPTION_KEY, "the-encryption-key");
        source.put(ConfigFields.BASE_URL, "http://localhost:1990/confluence");
        source.put(ConfigFields.USE_ATLASSIAN_ID, "false");
        source.put(ConfigFields.PLUGIN_ARTIFACT, "pluginArtifact");

        serializer.deserializeTaskContext(destination, task);

        assertEquals("admin", destination.get(UiFields.USERNAME));
        assertEquals(DefaultConfigurationSerializer.SAVED_PASSWORD_IDENTIFIER, destination.get(UiFields.PASSWORD));
        assertEquals("http://localhost:1990/confluence", destination.get(UiFields.BASE_URL));
        assertEquals("false", destination.get(UiFields.USE_ATLASSIAN_ID));
        assertEquals("pluginArtifact", destination.get(UiFields.PLUGIN_ARTIFACT));
    }
}
