package it.com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.build.logger.NullBuildLogger;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.UiFields;
import com.atlassian.bamboo.plugins.confdeploy.upload.http.DefaultHttpClientWrapper;
import com.atlassian.bamboo.plugins.confdeploy.upload.http.HttpClientWrapper;
import com.atlassian.bamboo.testutils.config.BambooEnvironmentData;
import com.atlassian.bamboo.testutils.junit.rule.BackdoorRule;
import com.atlassian.bamboo.webdriver.WebDriverTestEnvironmentData;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.upm.core.rest.MediaTypes;
import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * I'm the mother flippin' rhymenoceros
 * My beats are fat
 * And the birds are on my back
 * And I'm horny, I'm horny
 *
 * If you choose to proceed
 * You will indeed concede
 * Cause I hit you with my flow
 * The wild rhino stampede
 *
 * I'm not just wild, I'm trained, domesticated
 * I was raised by a rapper and rhino that dated
 * And subsequently procreated
 * That's how it goes
 *
 * Here's the Hiphopopotamus
 * The hip hop hippo
 * They call me the Hiphopopotamus
 * My lyrics are bottomless
 *
 * They call me the Hiphopopotamus
 * Flows that glow like phosphorous
 * Poppin' off the top of this esophagus
 * Rockin' this metropolis
 *
 * I'm not a large water-dwelling mammal
 * Where did you get that preposterous hypothesis?
 * Did Steve tell you that, perchance?
 * Steve!
 *
 * My rhymes and records, they don't get played
 * Because my records and rhymes, they don't get made
 * And if you rap like me you don't get paid
 * And if you roll like me you don't get laid
 *
 * My rhymes are so potent that in this small segment
 * I made all of the ladies in the area pregnant
 * Yes, sometimes my lyrics are sexist
 * But you lovely bitches and hoes should know
 * I'm trying to correct this
 *
 * Other rappers dis me
 * Say my rhymes are sissy
 * Why? Why? Why? What?
 * Why exactly?
 *
 * What? Why?
 * Be more constructive
 * With your feedback, please
 * Why? Why?
 *
 * Why? Cause I rap about reality
 * Like me and my grandma drinkin' a cup of tea
 * There ain't no party like my nanna's tea party
 * Hey, ho
 *
 * I'm the motherflippin'
 * I'm the motherflippin'
 * I'm the motherflippin'
 * Who's the motherflippin?
 *
 * I'm the motherflippin'
 * I'm the motherflippin'
 * I'm the motherflippin'
 * Motherflippin'
 *
 * http://www.youtube.com/watch?v=FArZxLj6DLk
 */
public class MotherFlippinSmokeTest
{
    private static final BambooEnvironmentData BAMBOO_TEST_ENVIRONMENT_DATA = new WebDriverTestEnvironmentData();
    private static final String CONFLUENCE_USERNAME = "admin";
    private static final String CONFLUENCE_PASSWORD = "admin";
    private static final String CONFLUENCE_PLUGIN_KEY = "com.atlassian.test.confluence-smoketest-plugin";
    private static final String CONFLUENCE_PLUGIN_VERSION = "1.0";

    @Rule public final BackdoorRule backdoor = new BackdoorRule(BAMBOO_TEST_ENVIRONMENT_DATA);
    @Rule public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock private TaskConfiguration config;

    private String confluenceBaseUrl;
    private String bambooBaseUrl;

    private HttpClientWrapper wrapper;

    @Before
    public void setUp()
    {
        this.confluenceBaseUrl = System.getProperty("http.confluence.url", "http://yeah-yeah.staff.sf.atlassian.com:1990/confluence");
        this.bambooBaseUrl = BAMBOO_TEST_ENVIRONMENT_DATA.getBaseUrl().toExternalForm();

        Mockito.when(config.getEnableTrafficLogging()).thenReturn(false);
        Mockito.when(config.allowSSLCertificateErrors()).thenReturn(false);

        this.wrapper = new DefaultHttpClientWrapper(new NullBuildLogger(), config);
    }

    @Test
    public void testConfluenceDeploy() throws Exception
    {
        // NOTE: using the HttpClientWrapper interface for the http requests adds a lot of clutter here, however it
        // does make the polling code that's necessary to wait for the bamboo build to finish very easy, and we need
        // to re-use the login session for that, so, sticking with it for now.

        // Ensure Confluence is running
        HttpPost confluenceLogin = new HttpPost(UrlUtils.join(confluenceBaseUrl, "/dologin.action"));
        confluenceLogin.setEntity(new UrlEncodedFormEntity(Arrays.asList(new BasicNameValuePair("os_username", CONFLUENCE_USERNAME),
                                                                         new BasicNameValuePair("os_password", CONFLUENCE_PASSWORD))));

        wrapper.execute(confluenceLogin, new ResponseHandler<Result>()
        {
            @Override
            public Result handleResponse(HttpResponse r) throws IOException
            {
                assertEquals("Confluence login failed. Make sure you run with the '-DtestGroup=smokeTests' parameter", 200, r.getStatusLine().getStatusCode());
                EntityUtils.consume(r.getEntity());

                return null;
            }
        });

        // Ensure that the smoke test plugin is not already installed in the Confluence instance (this would invalidate the smoke test)
        {
            final RequestSpecification requestSpec = confluenceRestRequestSpecBuilder()
                    .addHeader(HttpHeaders.ACCEPT, MediaTypes.INSTALLED_PLUGIN_JSON)
                    .addPathParam("pluginKey", CONFLUENCE_PLUGIN_KEY)
                    .build();
            final ResponseSpecification responseSpec = new ResponseSpecBuilder()
                    .expectStatusCode(HttpStatus.SC_NOT_FOUND)
                    .build();

            RestAssured.given(requestSpec, responseSpec).get("rest/plugins/1.0/{pluginKey}-key");
        }

        // Login to Bamboo
        HttpPost bambooLogin = new HttpPost(UrlUtils.join(bambooBaseUrl, "/userlogin.action"));
        bambooLogin.setEntity(new UrlEncodedFormEntity(Arrays.asList(new BasicNameValuePair("os_username", BAMBOO_TEST_ENVIRONMENT_DATA.getAuthenticatingUser().username),
                                                                     new BasicNameValuePair("os_password", BAMBOO_TEST_ENVIRONMENT_DATA.getAuthenticatingUser().password))));

        wrapper.execute(bambooLogin, new ResponseHandler<Result>()
        {
            @Override
            public Result handleResponse(HttpResponse r) throws IOException
            {
                assertEquals(200, r.getStatusLine().getStatusCode());
                EntityUtils.consume(r.getEntity());

                return null;
            }
        });

        // TERRIBLE, TERRIBLE HACK
        // The bamboo plan is configured to deploy to 'http://localhost:1990/confluence'. Unfortunately, the UPM REST API
        // requires that you communicate via the configured Base URL of the Confluence server, otherwise at one point
        // during the plugin installation process you will get redirected to a URL that contains the Base URL and your
        // authentication session (tracked by a cookie) will not get sent by the HttpClient and you'll get a HTTP 401
        // error.  Doubly sad, when you run Confluence using the Plugin SDK, the configured Base URL seems to always
        // get set to 'http://${LOCAL_HOST_NAME}/confluence'. This means we don't know what the Base URL of the Confluence
        // server is going to be until after the Confluence server has started, which means we can't pre-configure the
        // Bamboo task with the correct configuration until after the integration tests are running.  So, we'll need
        // to send a request to bamboo to update the task configuration with the correct Base URL, which we can find out
        // by querying Confluence 'getServerInfo' API method.
        HttpPost updateTaskConfiguration = new HttpPost(UrlUtils.join(bambooBaseUrl, "/build/admin/edit/updateTask.action"));
        updateTaskConfiguration.addHeader("Accept", "application/json");
        updateTaskConfiguration.setEntity(new UrlEncodedFormEntity(
                Arrays.asList(
                        // We have to post the entire task's configuration even just to update a single value. If the
                        // desired task configuration changes at all, these fields may need to be updated.
                        new BasicNameValuePair("userDescription", "Deploy Smoketest Plugin"),
                        new BasicNameValuePair("checkBoxFields", "taskDisabled"),
                        new BasicNameValuePair(UiFields.PLUGIN_ARTIFACT, "v2:688129:-1:-1:Confluence Smoketest Plugin"), // This is the encoded artifact String. See {@link AvailableArtifact} for deets.
                        new BasicNameValuePair("selectFields", "confDeployJar"),

                        // This is the Base URL being updated
                        new BasicNameValuePair(UiFields.BASE_URL, confluenceBaseUrl),

                        new BasicNameValuePair(UiFields.USERNAME, "admin"),
                        new BasicNameValuePair(UiFields.PASSWORD, "admin"),
                        new BasicNameValuePair(UiFields.PASSWORD_VARIABLE, ""),
                        new BasicNameValuePair(UiFields.ATLASSIAN_ID_BASE_URL, ""),
                        new BasicNameValuePair("bcpd_config_atlassianIdUsername", ""),
                        new BasicNameValuePair("bcpd_config_atlassianIdPassword", ""),
                        new BasicNameValuePair("bcpd_config_atlassianIdPasswordVariable", ""),
                        new BasicNameValuePair("bcpd_config_atlassianIdAppName", ""),
                        new BasicNameValuePair("enableTrafficLogging", "true"),

                        new BasicNameValuePair("checkBoxFields", "enableTrafficLogging"),
                        new BasicNameValuePair("pluginInstallationTimeout", "90"),
                        new BasicNameValuePair("createTaskKey", ""),
                        new BasicNameValuePair("taskId", "1"),
                        new BasicNameValuePair("planKey", "SMOKE-CST-DJ"),
                        new BasicNameValuePair("bamboo.successReturnMode", "json"),
                        new BasicNameValuePair("decorator", "nothing"),
                        new BasicNameValuePair("confirm", "true")
                )
        ));

        wrapper.execute(updateTaskConfiguration, new ResponseHandler<Result>()
        {
            @Override
            public Result handleResponse(HttpResponse response) throws IOException
            {
                // Expected success response looks like this:
                //{"status":"OK","taskResult":{"task":{"id":1,"name":"Deploy Confluence Plugin","description":"Deploy Smoketest Plugin","isEnabled":true,"valid":true},"warning":{}}}

                try
                {
                    assertEquals(200, response.getStatusLine().getStatusCode());

                    JSONObject j = new JSONObject(EntityUtils.toString(response.getEntity()));
                    assertEquals("OK", j.get("status"));
                }
                catch (JSONException e)
                {
                    fail(e.toString());
                }
                return null;
            }
        });

        // Trigger the Confluence smoke test build (SMOKE-CST) and ensure it was successful
        final PlanKey planKey = PlanKeys.getPlanKey("SMOKE-CST");
        backdoor.plans().triggerBuild(planKey);
        backdoor.plans().waitForSuccessfulBuild(PlanKeys.getPlanResultKey(planKey, 1), Poller.by(5, TimeUnit.MINUTES));

        // Ensure that the smoke test plugin is now installed in Confluence (using UPM API)
        {
            final RequestSpecification requestSpec = confluenceRestRequestSpecBuilder()
                    .addHeader(HttpHeaders.ACCEPT, MediaTypes.INSTALLED_PLUGIN_JSON)
                    .addPathParam("pluginKey", CONFLUENCE_PLUGIN_KEY)
                    .build();
            final ResponseSpecification responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(HttpStatus.SC_OK)
                .expectContentType(MediaTypes.INSTALLED_PLUGIN_JSON)
                .build();

            RestAssured.given(requestSpec, responseSpec).get("rest/plugins/1.0/{pluginKey}-key")
                    .then()
                    .assertThat()
                    .body("enabled", equalTo(true))
                    .body("version", equalTo(CONFLUENCE_PLUGIN_VERSION))
            ;
        }

        // And we're done! Now, was that so hard? ;)
    }

    private RequestSpecBuilder confluenceRestRequestSpecBuilder()
    {
        final PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
        authScheme.setUserName(CONFLUENCE_USERNAME);
        authScheme.setPassword(CONFLUENCE_PASSWORD);

        return new RequestSpecBuilder()
                .setBaseUri(confluenceBaseUrl)
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .addHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .setAuth(authScheme);
    }
}
