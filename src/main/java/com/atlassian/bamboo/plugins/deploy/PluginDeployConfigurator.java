package com.atlassian.bamboo.plugins.deploy;

import com.atlassian.bamboo.plugins.confdeploy.AutoDeployConfigurator;
import com.atlassian.bamboo.plugins.confdeploy.config.RemoteProductType;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.webwork.util.WwSelectOption;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Map;

/**
 * Task configurator for {@link com.atlassian.bamboo.plugins.deploy.PluginDeployTask}
 */
public class PluginDeployConfigurator extends AutoDeployConfigurator
{
    @Override
    protected RemoteProductType getProduct()
    {
        // Default?
        return RemoteProductType.JIRA;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        addProductTypes(context);
        context.put("multiProduct", true);

    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        addProductTypes(context);
        context.put("multiProduct", true);
    }

    private void addProductTypes(final Map<String, Object> context)
    {
        RemoteProductType[] all = RemoteProductType.values();
        ArrayList<WwSelectOption> allProducts = Lists.newArrayList(Iterables.transform(
                Lists.newArrayList(all),
                new Function<RemoteProductType, WwSelectOption>()
                {
                    @Override
                    public WwSelectOption apply(RemoteProductType remoteProductType)
                    {
                        return new WwSelectOption(
                                getI18nBeanForActionLocale().getText(remoteProductType.getProductKey()),
                                "",
                                remoteProductType.getProductKey()
                        );
                    }
                }
        ));
        context.put("allProducts", allProducts);
    }
}
