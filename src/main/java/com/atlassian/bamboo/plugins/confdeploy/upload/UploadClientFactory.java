package com.atlassian.bamboo.plugins.confdeploy.upload;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.confdeploy.config.RemoteProductType;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.*;

/**
 * Decides the appropriate combination of {@link RequestBehaviour} objects to be combined into an {@link DefaultUploadClient}
 * instance for communicating with the remote Atlassian application identified by a {@link RemoteProductType} and
 * {@link TaskConfiguration} instance.
 */
public interface UploadClientFactory
{
    /**
     * Constructs a new {@link DefaultUploadClient} instance.
     *
     * @param productType       Which Atlassian product are you talking to (JIRA, Confluence, etc.)?
     * @param taskConfiguration The task's runtime configuration.
     * @param buildLogger       Use it for logging messages, dummy.
     * @return The thing you asked for.
     */
    public DefaultUploadClient getUploadClient(final RemoteProductType productType, final TaskConfiguration taskConfiguration, final BuildLogger buildLogger);
}
