package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.PollingRequestBehaviour;
import com.atlassian.fugue.Either;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

import java.util.Map;
import java.util.Random;

/**
 * Special-case logic when deploying the UPM itself - polls the UPM REST API until it's back online again.
 */
public class UpmSelfUpdateCheckBehaviour  implements PollingRequestBehaviour
{
    private static final Random RAND = new Random();

    private final String baseUrl;

    public UpmSelfUpdateCheckBehaviour(final String baseUrl)
    {
        this.baseUrl = baseUrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<HttpUriRequest, Failure> getRequest(final TaskConfiguration configuration, final Map<String, Object> requestContext)
    {
       return Either.left(getRequestInternal());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<HttpUriRequest, Result> apply(HttpResponse response)
    {
        try
        {
            Thread.sleep(10000);
        }
        catch (InterruptedException e)
        {
            return Either.<HttpUriRequest, Result>right(Result.failure("Interrupted while waiting for UPM to self-update", e));
        }

        if (response.getStatusLine().getStatusCode() == 404)
        {
            // Keep polling
            return Either.left(getRequestInternal());
        }
        // Self-update is complete.
        return Either.<HttpUriRequest, Result>right(Result.success("UPM self-update is complete"));
    }

    private HttpUriRequest getRequestInternal()
    {
        String pollUrl = UrlUtils.join(baseUrl, "/rest/plugins/1.0/?_=" + RAND.nextLong());
        HttpGet pollGet = new HttpGet(pollUrl);
        pollGet.addHeader("Accept", "*/*");
        return pollGet;
    }
}
