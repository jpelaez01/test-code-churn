package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product;

import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ProductLoginRequestBehaviour;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Use me for logging in to FishEye/Crucible!
 */
public class FeCruLoginRequestBehaviour extends ProductLoginRequestBehaviour
{
    public FeCruLoginRequestBehaviour(final String baseUrl)
    {
        super(UrlUtils.join(baseUrl, "/login"));
    }

    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            // 302 or 200 indicates success. If 200, make sure the 'X-AUSERNAME' header is not anonymous.
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200)
            {
                String username = response.getFirstHeader("X-AUSERNAME").getValue();
                if (username.equals("anonymous"))
                {
                    return Result.failure("Product login failed - response username was anonymous");
                }
            }
            else if (statusCode != 302)
            {
                return Result.failure("Product login failed. HTTP Status code " + statusCode + " was returned.");
            }

            return Result.success("Product login success!");
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }

    }
}
