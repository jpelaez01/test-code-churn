package com.atlassian.bamboo.plugins.confdeploy.config.ui;

/**
 * HTML ID Attribute values for input fields in the task configuration form.
 */
public class UiFields
{
    // Other random fields
    public static final String EDIT_CREDENTIALS = "bcpd_config_credentials";

    // Actual fields whose values get persisted
    public static final String PRODUCT = "bcpd_config_product";
    public static final String PLUGIN_ARTIFACT = "bcpd_config_artifact";
    public static final String PLUGIN_ARTIFACT_NAME = "bcpd_config_artifact_name";
    public static final String BASE_URL = "bcpd_config_baseUrl";
    public static final String USERNAME = "bcpd_config_username";
    public static final String USE_PASSWORD_VARIABLE = "bcpd_config_usePasswordVariable";
    public static final String PASSWORD = "bcpd_config_password";
    public static final String PASSWORD_VARIABLE = "bcpd_config_passwordVariable";
    public static final String USE_ATLASSIAN_ID = "bcpd_config_useAtlassianId";
    public static final String ATLASSIAN_ID_USERNAME = "bcpd_config_atlassianIdUsername";
    public static final String ATLASSIAN_ID_PASSWORD = "bcpd_config_atlassianIdPassword";
    public static final String USE_ATLASSIAN_ID_PASSWORD_VARIABLE = "bcpd_config_useAtlassianIdPasswordVariable";
    public static final String ATLASSIAN_ID_PASSWORD_VARIABLE = "bcpd_config_atlassianIdPasswordVariable";
    public static final String USE_ATLASSIAN_ID_WEBSUDO = "bcpd_config_useAtlassianIdWebSudo";
    public static final String ATLASSIAN_ID_APP_NAME = "bcpd_config_atlassianIdAppName";
    public static final String ATLASSIAN_ID_BASE_URL = "bcpd_config_atlassianIdBaseURL";

    public static final String ALLOW_BRANCH_BUILDS = "bcpd_config_branching";
    public static final String ALLOW_SSL_ERRORS = "bcpd_config_ssl";
    public static final String ENABLE_TRAFFIC_LOGGING = "bcpd_config_logging";
    public static final String PLUGIN_INSTALLATION_TIMEOUT = "bcpd_config_timeout";
}
