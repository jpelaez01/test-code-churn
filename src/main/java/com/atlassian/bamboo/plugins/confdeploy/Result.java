package com.atlassian.bamboo.plugins.confdeploy;

import javax.annotation.Nullable;

/**
 *
 */
public abstract class Result
{
    public static Failure failure(String message)
    {
        return failure(message, null);
    }

    public static Failure failure(String message, @Nullable Exception e)
    {
        return new Failure(message, e);
    }

    public static Success success(String message)
    {
        return new Success(message);
    }

    public static Success success(String message, @Nullable Object context)
    {
        return new Success(message, context);
    }

    private final String message;
    private final Object context;

    public Result(String message, Object context)
    {
        this.message = message;
        this.context = context;
    }

    public Result(String message)
    {
        this.message = message;
        this.context = null;
    }

    public abstract boolean isSuccess();

    public String getMessage()
    {
        return message;
    }

    public Object getContext()
    {
        return context;
    }
}
