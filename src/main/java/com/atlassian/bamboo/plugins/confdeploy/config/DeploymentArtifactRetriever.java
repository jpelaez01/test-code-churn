package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.plugin.ArtifactDownloaderTaskConfigurationHelper;
import com.atlassian.bamboo.plugin.BambooPluginKeys;
import com.atlassian.bamboo.plugins.confdeploy.AvailableArtifact;
import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.task.runtime.RuntimeTaskDefinition;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.fugue.Either;

import java.io.File;
import java.util.List;

import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;

/**
 * Implements the {@link ArtifactRetriever} service for tasks within Deployment projects.
 */
public class DeploymentArtifactRetriever extends BaseArtifactRetriever implements ArtifactRetriever
{
    public DeploymentArtifactRetriever(final DeploymentTaskContext taskContext,
                                    final CustomVariableContext customVariableContext)
    {
        super(taskContext, customVariableContext);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<File, Failure> getFileFromArtifact(String artifactString)
    {
        AvailableArtifact artifact;

        if (!AvailableArtifact.isValidStoredArtifact(artifactString)) {
            final Either<AvailableArtifact, Failure> artifactResult = tryToRetieveArtifactByName(artifactString);

            if (artifactResult.isRight()) {
                return Either.right(artifactResult.right().get());
            }

            artifact = artifactResult.left().get();
        } else {
            artifact = new AvailableArtifact(artifactString);
        }

        if (!artifact.isFromTransferTask())
        {
            return Either.right(Result.failure("Configured plugin artifact was not acquired by a transfer task, but this is a deployment project. WTF?"));
        }

        // Ensure that the artifact exists and is accessible.
        File pluginJar = getArtifactFromTransferTask(artifact);
        if (pluginJar == null)
        {
            final String msg = String.format("Unable to find the artifact with id %d in this task's context. Has an artifact subscription been configured for that artifact in this job?", artifact.getArtifactId());
            return Either.right(Result.failure(msg));
        }

        if (!pluginJar.exists() || !pluginJar.isFile())
        {
            final String msg = String.format("Artifact file %s is either missing or not a file.", pluginJar.getAbsolutePath());
            return Either.right(Result.failure(msg));
        }
        return Either.left(pluginJar);

    }

    private Either<AvailableArtifact, Failure> tryToRetieveArtifactByName(String artifactName) {
        final Option<RuntimeTaskDefinition> artifactDownloaderTaskOptional = getArtifactDownloaderTask();

        if (!artifactDownloaderTaskOptional.isDefined()) {
            return Either.right(Result.failure("Found no artifact downloader task for this deployment."));
        }

        final RuntimeTaskDefinition taskDefinition = artifactDownloaderTaskOptional.get();
        final long artifactId = Long.parseLong(taskDefinition.getConfiguration().get(ArtifactDownloaderTaskConfigurationHelper.getArtifactIdKey(0)));

        final Iterable<String> artifactKeys = ArtifactDownloaderTaskConfigurationHelper.getArtifactKeys(taskDefinition.getConfiguration());
        final Option<String> artifactKey = Iterables.first(artifactKeys);

        if (!artifactKey.isDefined()) {
            final String msg = String.format("Could not find artifact for name %s", artifactName);
            return Either.right(Result.failure(msg));
        }

        final int transferId = ArtifactDownloaderTaskConfigurationHelper.getIndexFromKey(artifactKey.get());

        return Either.left(AvailableArtifact.fromTransferTask(artifactId, taskDefinition, transferId, artifactName));
    }

    private Option<RuntimeTaskDefinition> getArtifactDownloaderTask() {
        final List<RuntimeTaskDefinition> runtimeTaskDefinitions = taskContext.getCommonContext().getRuntimeTaskDefinitions();
        return Iterables.findFirst(runtimeTaskDefinitions, task -> task.getPluginKey().equals(BambooPluginKeys.ARTIFACT_DOWNLOAD_TASK_MODULE_KEY));
    }
}
