package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.fugue.Either;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

/**
 * Special-case logic when deploying the UPM itself - you need to trigger its self-update logic in the remote
 * Atlassian application.
 */
public class UpmSelfUpdateBehaviour implements RequestBehaviour
{
    private static final Random RAND = new Random();

    private final String baseUrl;

    public UpmSelfUpdateBehaviour(final String baseUrl)
    {
        this.baseUrl = baseUrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<HttpUriRequest, Failure> getRequest(final TaskConfiguration configuration, final Map<String, Object> requestContext)
    {
        final String requestUrl = UrlUtils.join(baseUrl, "/rest/plugins/self-update/1.0/?_=" + RAND.nextLong());
        return Either.<HttpUriRequest, Failure>left(new HttpPost(requestUrl));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != 201) //created
            {
                return Result.failure("UPM Self-update failed. HTTP status code " + statusCode + " was returned.");
            }
            return Result.success("UPM Self-update succeeded!");
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }
    }
}
