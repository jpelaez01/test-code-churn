package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.fugue.Either;
import org.apache.commons.lang.Validate;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Physically transfers the plugin artifact to the remote Atlassian application using the Universal Plugin Manager's
 * REST API.
 */
public class PluginUploadRequestBehaviour implements RequestBehaviour
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Either<HttpUriRequest, Failure> getRequest(final TaskConfiguration configuration, final Map<String, Object> requestContext)
    {
        Validate.notEmpty(configuration.getRemoteBaseUrl(), "Remote Base URL is required to upload plugins.");
        Validate.notNull(configuration.getPluginJar(), "An actual plugin artifact is required in order to upload a plugin artifact. Derp.");

        if (!requestContext.containsKey("upmToken"))
        {
            return Either.right(Result.failure("Could not start plugin upload - the UPM XSRF token is missing (probably a bug!)"));
        }
        String upmToken = requestContext.get("upmToken").toString();

        final String uploadUrl;
        try
        {
            uploadUrl = UrlUtils.join(configuration.getRemoteBaseUrl(), "/rest/plugins/1.0/?token=" + URLEncoder.encode(upmToken, "utf-8"));
        }
        catch (UnsupportedEncodingException e)
        {
            return Either.right(Result.failure("Failed to encode UTF-8 String - Java, you suck.", e));
        }

        HttpPost upmPost = new HttpPost(uploadUrl);
        upmPost.addHeader("Accept", "application/json");
        upmPost.setEntity(getPluginUploadEntity(configuration.getPluginJar()));

        return Either.<HttpUriRequest, Failure>left(upmPost);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != 202) // 202 ACCEPTED = all systems go
            {
                return Result.failure("Plugin upload failed. Unexpected HTTP status " + statusCode + " was returned.");
            }

            String entity = EntityUtils.toString(response.getEntity());
            try
            {
                return Result.success("Plugin upload succeeded!", new JSONObject(entity));
            }
            catch (JSONException e)
            {
                return Result.failure("Failed to JSONify the plugin upload response.", e);
            }
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }
    }

    private static HttpEntity getPluginUploadEntity(File plugin)
    {
        FileBody uploadFile = new FileBody(plugin);
        MultipartEntity uploadEntity = new MultipartEntity();
        uploadEntity.addPart("plugin", uploadFile);

        return uploadEntity;
    }
}
