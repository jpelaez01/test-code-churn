package com.atlassian.bamboo.plugins.confdeploy.config.ui;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plan.artifact.ImmutableArtifactSubscription;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask;
import com.atlassian.bamboo.plugins.confdeploy.AvailableArtifact;
import com.atlassian.bamboo.plugins.confdeploy.Crypto;
import com.atlassian.bamboo.task.TaskContextHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.fugue.Pair;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * {@inheritDoc}
 */
public class DefaultConfigurationSerializer implements ConfigurationSerializer
{
    static final String SAVED_PASSWORD_IDENTIFIER = "397ef670-6140-11e3-949a-0800200c9a66";

    private static final Logger log = LoggerFactory.getLogger(DefaultConfigurationSerializer.class);

    // TODO: A bit of a hack.... just use a Tuple here instead?
    private static class DeserializationInfo
    {
        private final String key;
        private final boolean isEncrypted;

        public DeserializationInfo(String key)
        {
            this.key = key;
            this.isEncrypted = false;
        }

        public DeserializationInfo(String key, boolean isEncrypted)
        {
            this.key = key;
            this.isEncrypted = isEncrypted;
        }

        public String getKey()
        {
            return this.key;
        }

        public boolean isEncrypted()
        {
            return this.isEncrypted;
        }
    }


    private static final Map<String, DeserializationInfo> FIELD_MAPPINGS;
    static {
        ImmutableMap.Builder<String, DeserializationInfo> b = ImmutableMap.builder();

        b.put(ConfigFields.PLUGIN_ARTIFACT, new DeserializationInfo(UiFields.PLUGIN_ARTIFACT));
        b.put(ConfigFields.BASE_URL, new DeserializationInfo(UiFields.BASE_URL));
        b.put(ConfigFields.USERNAME, new DeserializationInfo(UiFields.USERNAME));
        b.put(ConfigFields.USE_PASSWORD_VARIABLE, new DeserializationInfo(UiFields.USE_PASSWORD_VARIABLE));
        b.put(ConfigFields.PASSWORD, new DeserializationInfo(UiFields.PASSWORD, true));
        b.put(ConfigFields.PASSWORD_VARIABLE, new DeserializationInfo(UiFields.PASSWORD_VARIABLE));
        b.put(ConfigFields.USE_ATLASSIAN_ID, new DeserializationInfo(UiFields.USE_ATLASSIAN_ID));
        b.put(ConfigFields.ATLASSIAN_ID_USERNAME, new DeserializationInfo(UiFields.ATLASSIAN_ID_USERNAME));
        b.put(ConfigFields.ATLASSIAN_ID_PASSWORD, new DeserializationInfo(UiFields.ATLASSIAN_ID_PASSWORD, true));
        b.put(ConfigFields.USE_ATLASSIAN_ID_PASSWORD_VARIABLE, new DeserializationInfo(UiFields.USE_ATLASSIAN_ID_PASSWORD_VARIABLE));
        b.put(ConfigFields.ATLASSIAN_ID_PASSWORD_VARIABLE, new DeserializationInfo(UiFields.ATLASSIAN_ID_PASSWORD_VARIABLE));
        b.put(ConfigFields.USE_ATLASSIAN_ID_WEBSUDO, new DeserializationInfo(UiFields.USE_ATLASSIAN_ID_WEBSUDO));
        b.put(ConfigFields.ATLASSIAN_ID_APP_NAME, new DeserializationInfo(UiFields.ATLASSIAN_ID_APP_NAME));
        b.put(ConfigFields.ATLASSIAN_ID_BASE_URL, new DeserializationInfo(UiFields.ATLASSIAN_ID_BASE_URL));
        b.put(ConfigFields.ALLOW_BRANCH_BUILDS, new DeserializationInfo(UiFields.ALLOW_BRANCH_BUILDS));
        b.put(ConfigFields.ALLOW_SSL_ERRORS, new DeserializationInfo(UiFields.ALLOW_SSL_ERRORS));
        b.put(ConfigFields.ENABLE_TRAFFIC_LOGGING, new DeserializationInfo(UiFields.ENABLE_TRAFFIC_LOGGING));
        b.put(ConfigFields.PLUGIN_INSTALLATION_TIMEOUT, new DeserializationInfo(UiFields.PLUGIN_INSTALLATION_TIMEOUT));
        b.put(ConfigFields.PRODUCT_TYPE, new DeserializationInfo(UiFields.PRODUCT));

        FIELD_MAPPINGS = b.build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> serializeTaskContext(final TaskParametersMap params, final TaskDefinition taskDefinition)
    {
        final Map<String, String> config = Maps.newHashMap();

        config.put(ConfigFields.PRODUCT_TYPE, params.getProduct());
        config.put(ConfigFields.BASE_URL, params.getBaseUrl());
        config.put(ConfigFields.USERNAME, params.getUsername());
        config.put(ConfigFields.ALLOW_BRANCH_BUILDS, String.valueOf(params.getAllowBranchBuilds()));


        persistPassword(taskDefinition, params.getAll(), config,
                        UiFields.USE_PASSWORD_VARIABLE, UiFields.PASSWORD_VARIABLE, UiFields.PASSWORD,
                        ConfigFields.USE_PASSWORD_VARIABLE, ConfigFields.PASSWORD_VARIABLE, ConfigFields.PASSWORD, ConfigFields.PASSWORD_ENCRYPTION_KEY);

        boolean useAtlassianId = params.useAtlassianId();
        config.put(ConfigFields.USE_ATLASSIAN_ID, String.valueOf(useAtlassianId));
        if (useAtlassianId)
        {
            config.put(ConfigFields.ATLASSIAN_ID_BASE_URL, params.getAtlassianIdBaseUrl());
            config.put(ConfigFields.ATLASSIAN_ID_USERNAME, params.getAtlassianIdUsername());
            config.put(ConfigFields.USE_ATLASSIAN_ID_WEBSUDO, String.valueOf(params.useAtlassianIdWebSudo()));
            config.put(ConfigFields.ATLASSIAN_ID_APP_NAME, params.getAtlassianIdAppName());

            persistPassword(taskDefinition, params.getAll(), config,
                            UiFields.USE_ATLASSIAN_ID_PASSWORD_VARIABLE, UiFields.ATLASSIAN_ID_PASSWORD_VARIABLE, UiFields.ATLASSIAN_ID_PASSWORD,
                            ConfigFields.USE_ATLASSIAN_ID_PASSWORD_VARIABLE, ConfigFields.ATLASSIAN_ID_PASSWORD_VARIABLE, ConfigFields.ATLASSIAN_ID_PASSWORD, ConfigFields.ATLASSIAN_ID_PASSWORD_ENCRYPTION_KEY);
        }

        config.put(ConfigFields.PLUGIN_ARTIFACT, params.getArtifact());
        config.put(ConfigFields.ALLOW_SSL_ERRORS, String.valueOf(params.getAllowCertificateErrors()));
        config.put(ConfigFields.ENABLE_TRAFFIC_LOGGING, String.valueOf(params.getTrafficDump()));

        // Only store the installation timeout value if it is specified and not different to the default
        final int timeout = params.getPluginInstallationTimeout();
        if (timeout != -1 && timeout != AutoDeployTask.DEFAULT_PLUGIN_INSTALLATION_TIMEOUT)
        {
            config.put(ConfigFields.PLUGIN_INSTALLATION_TIMEOUT, String.valueOf(timeout));
        }

        return config;
    }

    /**
     * Persists a password to the destination config map, which may either be a password variable (which is persisted unmodified), a plain-text password (which is encrypted before persisting)
     * or a placeholder value indicating that the previously-persisted password should be re-used.
     */
    private void persistPassword(final TaskDefinition task, final ActionParametersMap source, final Map<String, String> destination,
                                 final String sourceVariableCheckKey, final String sourceVariableKey, final String sourcePasswordKey,
                                 final String destVariableCheckKey, final String destVariableKey, final String destPasswordKey, final String destEncryptionKeyKey)
    {
        final String rawPassword = source.getString(sourcePasswordKey);

        boolean persistPasswordVariable = source.containsKey(sourceVariableCheckKey) && source.getBoolean(sourceVariableCheckKey); // If a variable if present, it takes priority in serialization
        boolean persistPassword = source.containsKey(sourcePasswordKey) && StringUtils.isNotBlank(rawPassword); // Encrypt and persist the raw password, if present

        if (persistPasswordVariable)
        {
            destination.put(destVariableCheckKey, Boolean.toString(true));
            destination.put(destVariableKey, source.getString(sourceVariableKey));
            destination.put(destEncryptionKeyKey, "");
            destination.put(destPasswordKey, "");
        }
        else if (persistPassword)
        {
            if (rawPassword == null)
            {
                // This should never happen.
                throw new UnsupportedOperationException("Cannot persist empty password");
            }

            // Is this a password to be encrypted, or the place-holder indicating that the previously-persisted password should be re-used?
            if (rawPassword.equals(SAVED_PASSWORD_IDENTIFIER))
            {
                // Re-use the password from the prior configuration
                destination.put(destVariableCheckKey, Boolean.toString(false));
                destination.put(destVariableKey, "");
                destination.put(destEncryptionKeyKey, task.getConfiguration().get(destEncryptionKeyKey));
                destination.put(destPasswordKey, task.getConfiguration().get(destPasswordKey));
            }
            else
            {
                // Encrypt & save this new password
                final Pair<String, String> encryptedPassword = Crypto.encrypt(rawPassword);
                destination.put(destVariableCheckKey, Boolean.toString(false));
                destination.put(destVariableKey, "");
                destination.put(destEncryptionKeyKey, encryptedPassword.left());
                destination.put(destPasswordKey, encryptedPassword.right());
            }
        }
        else
        {
            destination.put(destVariableCheckKey, Boolean.toString(false));
            destination.put(destVariableKey, "");
            destination.put(destEncryptionKeyKey, "");
            destination.put(destPasswordKey, "");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deserializeTaskContext(final Map<String, Object> context, TaskDefinition taskDefinition)
    {
        upgradeArtifactPersistence(context, taskDefinition);

        for (String key : FIELD_MAPPINGS.keySet())
        {
            String value = taskDefinition.getConfiguration().get(key);

            DeserializationInfo info = FIELD_MAPPINGS.get(key);
            if (info.isEncrypted() && !StringUtils.isBlank(value))
            {
                context.put(info.getKey(), SAVED_PASSWORD_IDENTIFIER);
            }
            else
            {
                context.put(info.getKey(), value);
            }
        }
    }

    private void upgradeArtifactPersistence(final Map<String, Object> context, final TaskDefinition taskDefinition)
    {
        final ImmutableJob job = Narrow.reinterpret(TaskContextHelper.getPlan(context), ImmutableJob.class);
        if (job == null)
            // If this is a deployment project, not a job, then it's not possible for the artifact configuration to be
            // serialised in the old format - the new serialisation was introduced when the task started supporting Deployment projects.
            return;

        String persistedArtifactName = taskDefinition.getConfiguration().get(ConfigFields.PLUGIN_ARTIFACT);
        if (persistedArtifactName.startsWith("v2:"))
            // Already using the new persistence format... skip it.
            return;

        // Find the artifact by name.
        String newPersistedArtifactName = null;
        for (ImmutableArtifactSubscription subscription : job.getArtifactSubscriptions())
        {
            if (subscription.getArtifactDefinition().getName().equals(persistedArtifactName))
            {
                newPersistedArtifactName = AvailableArtifact.from(subscription).toString();
                break;
            }
        }

        if (newPersistedArtifactName == null)
        {
            // TODO: Probably need to take more severe action here.
            log.error(String.format("Unable to locate artifact subscription named %s - the stored artifact format could not be updated. You probably won't be able to save this Task.", persistedArtifactName));
        }
        else
        {
            log.debug(String.format("Updating Artifact Subscription details from %s to %s", persistedArtifactName, newPersistedArtifactName));
            taskDefinition.getConfiguration().put(ConfigFields.PLUGIN_ARTIFACT, newPersistedArtifactName);
        }
    }
}
