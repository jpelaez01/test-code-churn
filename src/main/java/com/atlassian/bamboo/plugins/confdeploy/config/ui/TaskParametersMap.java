package com.atlassian.bamboo.plugins.confdeploy.config.ui;

import com.atlassian.bamboo.collections.ActionParametersMap;

/**
 * Encapsulates some of the tedium of getting field values out of the action parameters map.
 */
public  class TaskParametersMap
{
    private final ActionParametersMap params;

    public TaskParametersMap(final ActionParametersMap params)
    {
        this.params = params;
    }

    public ActionParametersMap getAll()
    {
        return params;
    }

    public String getProduct()
    {
        return params.getString(UiFields.PRODUCT);
    }

    public String getArtifact()
    {
        return params.getString(UiFields.PLUGIN_ARTIFACT);
    }

    public String getAllowCertificateErrors()
    {
        return params.getString(UiFields.ALLOW_SSL_ERRORS);
    }

    public String getBaseUrl()
    {
        return params.getString(UiFields.BASE_URL);
    }

    public String getUsername()
    {
        return params.getString(UiFields.USERNAME);
    }

    public String getPassword()
    {
        return params.getString(UiFields.PASSWORD);
    }

    public boolean usePasswordVariable()
    {
        return params.getBoolean(UiFields.USE_PASSWORD_VARIABLE);
    }

    public String getPasswordVariable()
    {
        return params.getString(UiFields.PASSWORD_VARIABLE);
    }

    public boolean useAtlassianId()
    {
        return params.getBoolean(UiFields.USE_ATLASSIAN_ID);
    }

    public String getAtlassianIdBaseUrl()
    {
        return params.getString(UiFields.ATLASSIAN_ID_BASE_URL);
    }

    public String getAtlassianIdUsername()
    {
        return params.getString(UiFields.ATLASSIAN_ID_USERNAME);
    }

    public String getAtlassianIdPassword()
    {
        return params.getString(UiFields.ATLASSIAN_ID_PASSWORD);
    }

    public boolean useAtlassianIdPasswordVariable()
    {
        return params.getBoolean(UiFields.USE_ATLASSIAN_ID_PASSWORD_VARIABLE);
    }

    public String getAtlassianIdPasswordVariable()
    {
        return params.getString(UiFields.ATLASSIAN_ID_PASSWORD_VARIABLE);
    }

    public boolean useAtlassianIdWebSudo()
    {
        return params.getBoolean(UiFields.USE_ATLASSIAN_ID_WEBSUDO);
    }

    public String getAtlassianIdAppName()
    {
        return params.getString(UiFields.ATLASSIAN_ID_APP_NAME);
    }

    public boolean getAllowBranchBuilds()
    {
        return params.getBoolean(UiFields.ALLOW_BRANCH_BUILDS);
    }

    public boolean getTrafficDump()
    {
        return params.getBoolean(UiFields.ENABLE_TRAFFIC_LOGGING);
    }

    public int getPluginInstallationTimeout()
    {
        return params.getInt(UiFields.PLUGIN_INSTALLATION_TIMEOUT, -1);
    }
}
