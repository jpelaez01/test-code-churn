package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.fugue.Maybe;
import com.atlassian.fugue.Option;

import javax.annotation.Nullable;
import java.io.File;

/**
 * {@inheritDoc}
 */
public class DefaultTaskConfiguration implements TaskConfiguration
{
    @Nullable
    private final RemoteProductType productType;
    private final File pluginJar;
    private final String remoteBaseUrl;
    private final String username;
    private final String password;
    private final boolean useAtlassianId;
    private final boolean useAtlassianIdWebSudo;
    private final String atlassianIdUsername;
    private final String atlassianIdPassword;
    private final String atlassianIdBaseUrl;
    private final String atlassianIdAppName;
    private final boolean allowSSLCertificateErrors;
    private final boolean allowBranchBuildExecution;
    private final boolean enableTrafficLogging;
    private final int pluginInstallationTimeout;

    /**
     * Don't call directly - use {@link TaskConfigurationFactory#getTaskConfiguration(com.atlassian.bamboo.configuration.ConfigurationMap)} )}
     */
    DefaultTaskConfiguration(@Nullable final RemoteProductType productType, final File pluginJar, final String remoteBaseUrl,
                             final String username, final String password, final boolean useAtlassianId,
                             final boolean useAtlassianIdWebSudo, final String atlassianIdUsername,
                             final String atlassianIdPassword, final String atlassianIdBaseUrl, final String atlassianIdAppName,
                             final boolean allowSSLCertificateErrors, final boolean allowBranchBuildExecution,
                             final boolean enableTrafficLogging, final int pluginInstallationTimeout)
    {
        this.productType = productType;
        this.pluginJar = pluginJar;
        this.remoteBaseUrl = remoteBaseUrl;
        this.username = username;
        this.password = password;
        this.useAtlassianId = useAtlassianId;
        this.useAtlassianIdWebSudo = useAtlassianIdWebSudo;
        this.atlassianIdUsername = atlassianIdUsername;
        this.atlassianIdPassword = atlassianIdPassword;
        this.atlassianIdBaseUrl = atlassianIdBaseUrl;
        this.atlassianIdAppName = atlassianIdAppName;
        this.allowSSLCertificateErrors = allowSSLCertificateErrors;
        this.allowBranchBuildExecution = allowBranchBuildExecution;
        this.enableTrafficLogging = enableTrafficLogging;
        this.pluginInstallationTimeout = pluginInstallationTimeout;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Maybe<RemoteProductType> getProduct()
    {
        if (productType == null)
        {
            return Option.none();
        }
        return Option.some(productType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getPluginJar()
    {
        return pluginJar;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRemoteBaseUrl()
    {
        return remoteBaseUrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUsername()
    {
        return username;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPassword()
    {
        return password;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean useAtlassianId()
    {
        return useAtlassianId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean useAtlassianIdWebSudo()
    {
        return useAtlassianIdWebSudo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAtlassianIdUsername()
    {
        return atlassianIdUsername;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAtlassianIdPassword()
    {
        return atlassianIdPassword;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAtlassianIdBaseUrl()
    {
        return atlassianIdBaseUrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAtlassianIdAppName()
    {
        return atlassianIdAppName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean allowSSLCertificateErrors()
    {
        return allowSSLCertificateErrors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean allowBranchBuildExecution()
    {
        return allowBranchBuildExecution;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getEnableTrafficLogging()
    {
        return enableTrafficLogging;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPluginInstallationTimeout()
    {
        return pluginInstallationTimeout;
    }
}
