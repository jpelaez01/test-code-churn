package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.fugue.Either;

import java.io.File;

/**
 * Locates a Bamboo artifact based on a serialized configuration string. The format of the configuration string is
 * determined by the {@link com.atlassian.bamboo.plugins.confdeploy.AvailableArtifact} class.
 *
 * If the Bamboo artifact is valid and can be resolved, this {@link ArtifactRetriever} then de-references the artifact
 * and locates the corresponding {@link File} on the file system that the artifact represents.
 */
public interface ArtifactRetriever
{
    /**
     * Locates the {@link File} contained within an artifact defined by the supplied artifact string.
     *
     * @param artifactString The serialized artifact identifier
     * @return Either the File handle for the referenced artifact, or a failure condition.
     */
    Either<File, Failure> getFileFromArtifact(final String artifactString);
}
