package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.bamboo.plan.artifact.ArtifactContext;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionContext;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionContext;
import com.atlassian.bamboo.plugins.confdeploy.AvailableArtifact;
import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.google.common.base.Predicate;

import java.io.File;

/**
 * Implements the {@link ArtifactRetriever} service for tasks within a Build job.
 */
public class JobArtifactRetriever extends BaseArtifactRetriever implements ArtifactRetriever
{
    private final ArtifactContext artifactContext;

    public JobArtifactRetriever(final TaskContext taskContext, final CustomVariableContext customVariableContext)
    {
        super(taskContext, customVariableContext);
        this.artifactContext = taskContext.getBuildContext().getArtifactContext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<File, Failure> getFileFromArtifact(final String artifactString)
    {
        AvailableArtifact artifactDesc;

        // May need to update the stored artifact definition to a newer format.
        if (!AvailableArtifact.isValidStoredArtifact(artifactString))
        {
            // The artifact configuration was serialized in the old format, where it is identified by name.
            Option<ArtifactSubscriptionContext> maybeArtifact = Iterables.findFirst(artifactContext.getSubscriptionContexts(), new Predicate<ArtifactSubscriptionContext>()
            {
                @Override
                public boolean apply(ArtifactSubscriptionContext artifactSubscriptionContext)
                {
                    final ArtifactDefinitionContext artifactDefinitionContext = artifactSubscriptionContext.getArtifactDefinitionContext();
                    return artifactDefinitionContext.getName().equals(artifactString);

                }
            });
            if (maybeArtifact.isEmpty())
            {
                return Either.right(Result.failure("Unable to locate Artifact subscription named " + artifactString));
            }
            else
            {
                artifactDesc = AvailableArtifact.from(maybeArtifact.get());
            }
        }
        else
        {
            artifactDesc = new AvailableArtifact(artifactString);
        }

        File pluginJar;
        if (artifactDesc.isFromTransferTask())
        {
            pluginJar = getArtifactFromTransferTask(artifactDesc);
        }
        else
        {
            // Ensure that the artifact exists and is accessible.
            pluginJar = getArtifactFromJob(artifactDesc);
        }

        if (pluginJar == null)
        {
            final String msg = String.format("Unable to find the artifact with id %d in this task's context. " +
                    "Has an artifact subscription been configured for that artifact in this job?", artifactDesc.getArtifactId());
            return Either.right(Result.failure(msg));
        }

        if (!pluginJar.exists() || !pluginJar.isFile())
        {
            final String msg = String.format("Artifact file %s is either missing or not a file.", pluginJar.getAbsolutePath());
            return Either.right(Result.failure(msg));
        }
        return Either.left(pluginJar);
    }


}
