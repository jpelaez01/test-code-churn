package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product;

import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ProductLoginRequestBehaviour;

/**
 * Use me for logging in to Confluence!
 */
public class ConfluenceLoginRequestBehaviour extends ProductLoginRequestBehaviour
{
    public ConfluenceLoginRequestBehaviour(final String baseUrl)
    {
        super(UrlUtils.join(baseUrl, "/dologin.action"));
    }
}
