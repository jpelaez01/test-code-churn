package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionContext;
import com.google.common.base.MoreObjects;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.plan.artifact.ImmutableArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ImmutableArtifactSubscription;
import com.atlassian.bamboo.task.TaskDefinition;
import com.google.common.base.Joiner;

public class AvailableArtifact {

    private static final Integer NO_TRANSFER_ID = -1;

    private final Long artifactId;
    private final Long artifactDownloaderTaskId;
    private final Integer artifactDownloaderTransferId;
    private final String name;

    public AvailableArtifact(String configEntry) {
        if (!isValidStoredArtifact(configEntry)) {
            throw new IllegalArgumentException("Not a valid artifact configuration string: " + configEntry);
        }

        final String[] data = configEntry.split(":", 5);
        artifactId = Long.valueOf(data[1]);
        Integer transferId = Integer.valueOf(data[3]);
        if (transferId.equals(NO_TRANSFER_ID)) {
            artifactDownloaderTaskId = null;
            artifactDownloaderTransferId = null;
        } else {
            artifactDownloaderTaskId = Long.valueOf(data[2]);
            artifactDownloaderTransferId = transferId;
        }

        name = data[4];
    }

    public Long getArtifactId() {
        return artifactId;
    }

    public String getName() {
        return name;
    }

    public Integer getArtifactDownloaderTransferId() {
        return artifactDownloaderTransferId;
    }

    public Long getArtifactDownloaderTaskId() {
        return artifactDownloaderTaskId;
    }

    private AvailableArtifact(long artifactIdOrType, @Nullable Long artifactDownloaderTaskId,
                              @Nullable Integer artifactDownloaderTransferId, String name) {
        this.artifactId = artifactIdOrType;
        this.artifactDownloaderTaskId = artifactDownloaderTaskId;
        this.artifactDownloaderTransferId = artifactDownloaderTransferId;
        this.name = name;
    }

    public static AvailableArtifact from(ArtifactSubscriptionContext artifactSubscriptionContext) {
        return new AvailableArtifact(artifactSubscriptionContext.getArtifactDefinitionContext().getId(), null, null,
                artifactSubscriptionContext.getArtifactDefinitionContext().getName());
    }

    public static AvailableArtifact from(ImmutableArtifactSubscription artifactSubscription) {
        return new AvailableArtifact(artifactSubscription.getArtifactDefinition().getId(), null, null,
                artifactSubscription.getName());
    }

    public static AvailableArtifact from(ImmutableArtifactDefinition artifactDefinition) {
        return new AvailableArtifact(artifactDefinition.getId(), null, null, artifactDefinition.getName());
    }

    public static AvailableArtifact fromTransferTask(long artifactId, TaskDefinition task, int transferId,
                                                     String artifactName) {
        return new AvailableArtifact(artifactId, task.getId(), transferId, artifactName);
    }

    public static boolean isValidStoredArtifact(String availableArtifactString) {
        return availableArtifactString.matches("^v2:((-)?\\d+:){3}.*");
    }

    @Override
    public String toString() {
        String joined = Joiner.on(':').join("v2", artifactId, MoreObjects.firstNonNull(artifactDownloaderTaskId, NO_TRANSFER_ID),
                MoreObjects.firstNonNull(artifactDownloaderTransferId, NO_TRANSFER_ID), name);
        return joined;
    }

    public boolean isFromTransferTask() {
        return artifactDownloaderTaskId != null;
    }
}
