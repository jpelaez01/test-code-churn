package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.fugue.Either;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

/**
 * Knows how to obtain an XSRF Token for posting to the plugin's managers REST resource for uploading plugins.
 */
public class UpmTokenRequestBehaviour implements RequestBehaviour
{
    private static final String UPM_TOKEN_HEADER = "upm-token";
    private static final Random RAND = new Random();

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<HttpUriRequest, Failure> getRequest(final TaskConfiguration configuration, final Map<String, Object> requestContext)
    {
        final String requestUrl = UrlUtils.join(configuration.getRemoteBaseUrl(), "/rest/plugins/1.0/?_=" + RAND.nextLong());
        HttpGet upmGet = new HttpGet(requestUrl);
        upmGet.addHeader("Accept", "application/vnd.atl.plugins.installed+json"); // UPM returns custom JSON content types.

        return Either.<HttpUriRequest, Failure>left(upmGet);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 401)
            {
                // If we get this far into the process only to receive a HTTP 401 (Unauthorized) now, 99% of the time
                // it's going to mean that the authenticating user is not an administrator.
                return Result.failure("Unauthorized attempt to obtain XSRF token from plugin manager. Your configured user account is not a system administrator in the remote system.");
            }

            Header[] tokenHeaders = response.getHeaders(UPM_TOKEN_HEADER);
            if (tokenHeaders == null || tokenHeaders.length != 1)
            {
                return Result.failure("Attempt to obtain XSRF token from plugin manager failed - the token was missing from the response. Has the plugin manager been disabled on the remote system?");
            }
            String upmToken = tokenHeaders[0].getValue();
            return Result.success("", upmToken);
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }
    }
}
