package com.atlassian.bamboo.plugins.confdeploy;


import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;
import com.atlassian.bamboo.plugins.confdeploy.config.RemoteProductType;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.ArtifactSubscriber;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.ConfigurationSerializer;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.DefaultConfigurationValidator;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.SubscribedArtifact;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.TaskParametersMap;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.UiFields;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.utils.i18n.I18nBean;
import com.atlassian.bamboo.utils.i18n.I18nBeanFactory;
import com.atlassian.bamboo.webwork.util.WwSelectOption;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.opensymphony.xwork.ActionContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Implements the logic for editing and configuring the task.
 */
public abstract class AutoDeployConfigurator extends AbstractTaskConfigurator implements TaskConfigurator
{
    private static final Logger log = LoggerFactory.getLogger(AutoDeployConfigurator.class);

    private ArtifactSubscriber artifactSubscriber;
    private ConfigurationSerializer configurationSerializer;
    private WebResourceManager webResourceManager;
    private ArtifactDefinitionManager artifactDefinitionManager;
    private I18nBeanFactory i18nBeanFactory;

    protected abstract RemoteProductType getProduct();

    /**
     * Converts the user's selected configuration into a persistable String/String map - this is the configuration map
     * that will be passed into the task when it executes.
     *
     * The {@link AutoDeployConfigurator#validate(com.atlassian.bamboo.collections.ActionParametersMap, com.atlassian.bamboo.utils.error.ErrorCollection)}
     * method is called, and must have succeeded, before this method is called.
     *
     * @param params                 The parameters entered in by the user.
     * @param previousTaskDefinition The previously-serialized configuration for this task.
     * @return A serialized String:String map of the new task configuration.
     */
    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        return configurationSerializer.serializeTaskContext(new TaskParametersMap(params), previousTaskDefinition);
    }

    /**
     * Populates the default configuration for a new instance of the task (ie. this is where you should populate the
     * context with sensible default values)
     */
    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        // Populate artifacts
        List<WwSelectOption> artifacts = addArtifactData(context);

        // Generate default values.
        if (artifacts.size() > 0)
            context.put(UiFields.PLUGIN_ARTIFACT, artifacts.get(0)); // Select an artifact by default

        // Ensure boolean values are initialized to the correct default value
        context.put(UiFields.ALLOW_BRANCH_BUILDS, String.valueOf(false));
        context.put(UiFields.USE_ATLASSIAN_ID, String.valueOf(false));
        context.put(UiFields.USE_ATLASSIAN_ID_WEBSUDO, String.valueOf(false));
        context.put(UiFields.ALLOW_SSL_ERRORS, String.valueOf(false));
        context.put(UiFields.ENABLE_TRAFFIC_LOGGING, String.valueOf(false));

        String webResources = webResourceManager.getResourceTags("com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:taskResources", UrlMode.RELATIVE);
        context.put("webResources", webResources);
        context.put("product", getProduct());
        context.put("state", "create");
        context.put("multiProduct", false);
    }

    /**
     * Populates the configuration with its previously persisted config (ie. this is where you implement re-loading a
     * saved configuration).
     */
    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        configurationSerializer.deserializeTaskContext(context, taskDefinition);
        addArtifactData(context);

        String webResources = webResourceManager.getResourceTags("com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:taskResources", UrlMode.RELATIVE);
        context.put("webResources", webResources);
        context.put("state", "edit");
        context.put("multiProduct", false);

        // TODO: Should probably duplicate all this for the view context as well.
        context.put("product", getProduct());
        if (context.containsKey(UiFields.PLUGIN_ARTIFACT) && context.get(UiFields.PLUGIN_ARTIFACT) != null && AvailableArtifact.isValidStoredArtifact(context.get(UiFields.PLUGIN_ARTIFACT).toString()))
        {
            context.put(UiFields.PLUGIN_ARTIFACT_NAME, new AvailableArtifact(context.get(UiFields.PLUGIN_ARTIFACT).toString()).getName());
        }

        Object useAtlassianId = context.get(UiFields.USE_ATLASSIAN_ID);
        if (useAtlassianId != null && Boolean.valueOf(useAtlassianId.toString()))
        {
            context.put("storedUser", Lists.newArrayList(context.get(UiFields.ATLASSIAN_ID_USERNAME)));
        }
        else
        {
            context.put("storedUser", Lists.newArrayList(context.get(UiFields.USERNAME)));
        }
    }

    /**
     * Called when the configuration is saved by the user - ensures that the selected configuration is valid. Add new errors
     * if it is not.
     */
    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        new DefaultConfigurationValidator(getI18nBeanForActionLocale(), artifactDefinitionManager).validate(new TaskParametersMap(params), errorCollection);
    }

    private List<WwSelectOption> addArtifactData(final Map<String, Object> context)
    {
        Iterable<SubscribedArtifact> subscriptions = artifactSubscriber.getSubscriptions(context);
        ArrayList<WwSelectOption> artifacts = Lists.newArrayList(Iterables.transform(
                subscriptions,
                new Function<SubscribedArtifact, WwSelectOption>()
                {
                    @Override
                    public WwSelectOption apply(SubscribedArtifact subscribedArtifact)
                    {
                        return new WwSelectOption(
                                subscribedArtifact.getDisplayName(),
                                subscribedArtifact.getGroup(),
                                subscribedArtifact.getId()
                        );
                    }
                }
        ));
        context.put("artifacts", artifacts);
        return artifacts;
    }

    protected I18nBean getI18nBeanForActionLocale()
    {
        Locale locale = ActionContext.getContext().getLocale();
        return i18nBeanFactory.getI18nBean(locale);
    }

    public void setArtifactSubscriber(final ArtifactSubscriber artifactSubscriber)
    {
        this.artifactSubscriber = artifactSubscriber;
    }

    public void setConfigurationSerializer(final ConfigurationSerializer configurationSerializer)
    {
        this.configurationSerializer = configurationSerializer;
    }

    public void setWebResourceManager(final WebResourceManager webResourceManager)
    {
        this.webResourceManager = webResourceManager;
    }

    public void setArtifactDefinitionManager(final ArtifactDefinitionManager artifactDefinitionManager)
    {
        this.artifactDefinitionManager = artifactDefinitionManager;
    }

    public void setI18nBeanFactory(final I18nBeanFactory i18nBeanFactory)
    {
        this.i18nBeanFactory = i18nBeanFactory;
    }
}
