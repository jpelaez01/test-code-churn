package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.fugue.Either;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.Validate;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Map;

/**
 * Base class for crafting a login POST request to an Atlassian product, since they're all pretty much the same, except:
 * - The login URL is different for each product, and
 * - The username and password are submitted using different parameter names.
 */
public abstract class ProductLoginRequestBehaviour implements RequestBehaviour
{
    private final boolean useQueryParams;
    private final String loginUrl;

    private final String usernameParameterName;
    private final String passwordParameterName;

    public ProductLoginRequestBehaviour(final String loginUrl)
    {
        this.loginUrl = loginUrl;
        useQueryParams = false;
        usernameParameterName = null;
        passwordParameterName = null;
    }

    public ProductLoginRequestBehaviour(final String loginUrl, String usernameParameterName, final String passwordParameterName) {
        this.loginUrl = loginUrl;
        useQueryParams = true;
        this.usernameParameterName = usernameParameterName;
        this.passwordParameterName = passwordParameterName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<HttpUriRequest, Failure> getRequest(TaskConfiguration configuration, Map<String, Object> requestContext)
    {
        Validate.notEmpty(configuration.getUsername(), "Username must be specified for login to succeed.");
        Validate.notEmpty(configuration.getPassword(), "Password must be specified for login to succeed.");

        HttpPost loginRequest = new HttpPost(loginUrl);
        loginRequest.addHeader("X-Atlassian-Token", "no-check");

        if (useQueryParams)
        {
            UrlEncodedFormEntity entity;
            try
            {
                entity = new UrlEncodedFormEntity(Lists.<NameValuePair>newArrayList(
                        new BasicNameValuePair(usernameParameterName, configuration.getUsername()),
                        new BasicNameValuePair(passwordParameterName, configuration.getPassword())

                ));

            }
            catch(UnsupportedEncodingException e)
            {
                return Either.right(Result.failure("Failed to encode UTF8 String  Java, you suck.", e));
            }
            loginRequest.setEntity(entity);
        }
        else
        {
            String passwordString = configuration.getUsername() + ":" + configuration.getPassword();
            String basicAuthHeader = Base64.getEncoder().encodeToString(passwordString.getBytes());
            loginRequest.addHeader("Authorization", "Basic " + basicAuthHeader);
        }

        return Either.<HttpUriRequest, Failure>left(loginRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != 200 && statusCode != 302)
            {
                return Result.failure("Product login failed. HTTP Status code " + statusCode + " was returned.");
            }

            Header[] headers = response.getHeaders("X-Seraph-LoginReason");
            if (headers == null || headers.length == 0) {
                return Result.failure("Product login failed.  The 'X-Seraph-LoginReason' header was missing from the response.");
            } else {
                Optional<Header> ok = Iterables.tryFind(Arrays.asList(headers), new Predicate<Header>()
                {
                    @Override
                    public boolean apply(Header header)
                    {
                        return header.getValue().equals("OK");
                    }
                });

                if (!ok.isPresent())
                {
                    return Result.failure("Product login failed. The X-Seraph-LoginReason response was " + headers[0].getValue() + ".");
                }
            }

            return Result.success("Product login success!");
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }
    }
}
