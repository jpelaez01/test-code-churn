package com.atlassian.bamboo.plugins.confdeploy.upload.http;

import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.fugue.Either;
import com.atlassian.util.concurrent.Timeout;
import com.google.common.base.Function;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;

/**
 * I'm not entirely convinced this interface is necessary. Abstracts the Apache HTTPClient dependency away from the
 * previously monolithic DefaultUploadClient ... kinda... it still leaks some classes to save on the effort of
 * duplicating them.
 *
 * This service is also responsible for:
 * - Managing session state and persisting cookies between requests.
 * - Configuring the HTTPClient to ignore server certificate errors (if that configuration option is selected)
 * - Handling the boilerplate code involved in polling a remote HTTP resource until a timeout is expired
 */
public interface HttpClientWrapper
{
    /**
     * Executes a single HTTP request and inspects the returned HTTP Response to determine the {@link Result} of the
     * request.
     *
     * @param request The HTTP Request to execute. Typically constructed through an implementation of {@link com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour}.
     * @param responseHandler A handler that is invoked if the HTTP request was successfully sent and the response was
     *                        successfully received. The responseHandler determines if the request was successful, and
     *                        may also extract any information from the server response that may be required by other
     *                        sections of code.
     *
     * @return A {@link Result} instance that confirms whether the request was successful or not. If the {@link Result#isSuccess()}
     *         value returns {@code true}, then the {@link Result#getContext()} method may optionally contain additional
     *         information about the response (such as some information from the response body). If the {@link Result#isSuccess()}
     *         value is {@code false}, then the {@link Result#getContext()} may return an {@link Exception} object that
     *         caused the failure.
     */
    Result execute(final HttpUriRequest request, final ResponseHandler<Result> responseHandler);

    /**
     * Makes repeated HTTP requests until a result is ascertained or a Timeout period is exceeded.
     *
     * @param initialRequest The first request that will be made. Once this request is made (using the same semantics as the
     *                       {@link #execute(HttpUriRequest, ResponseHandler)} method, the 'evaluator' will be invoked
     *                       to process the response. If the result of the initial request is that polling should not
     *                       proceed, a {@link Result} instance should be returned. If the result of the initial request
     *                       is that polling should proceed, then a new {@link HttpUriRequest} instance should be returned,
     *                       which will become the next request to be invoked by the polling logic. This request then
     *                       becomes the next 'initialRequest'.
     * @param evaluator A function that inspects HTTP responses and determines whether or not polling should stop or
     *                  continue. A {@link Result} instance is returned to stop polling (with either a successful or
     *                  unsuccessful outcome). A {@link HttpUriRequest} instance is returned to continue polling.
     * @param timeout The maximum amount of time that the polling may proceed before being terminated by the
     *                polling logic - a failure {@link Result} is returned.
     *
     * @return A {@link Result} instance that confirms whether the polling was successful or not. If the {@link Result#isSuccess()}
     *         value returns {@code true}, then the {@link Result#getContext()} method may optionally contain additional
     *         information about the response (such as some information from the response body). If the {@link Result#isSuccess()}
     *         value is {@code false}, then the {@link Result#getContext()} may return an {@link Exception} object that
     *         caused the failure.
     */
    Result poll(final HttpUriRequest initialRequest, final Function<HttpResponse,Either<HttpUriRequest, Result>> evaluator, final Timeout timeout);

}
