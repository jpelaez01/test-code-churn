package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionContext;
import com.atlassian.bamboo.plugin.ArtifactDownloaderTaskConfigurationHelper;
import com.atlassian.bamboo.plugins.confdeploy.AvailableArtifact;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.runtime.RuntimeTaskDefinition;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.utils.FileVisitor;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.bamboo.variable.substitutor.VariableSubstitutor;
import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Knows how to access artifacts on the file system during a build plan's execution.
 */
public abstract class BaseArtifactRetriever implements ArtifactRetriever
{
    private static final Logger log = LoggerFactory.getLogger(BaseArtifactRetriever.class);

    protected final CustomVariableContext variableContext;
    protected final CommonTaskContext taskContext;

    protected BaseArtifactRetriever(final CommonTaskContext taskContext, final CustomVariableContext variableContext)
    {
        this.taskContext = taskContext;
        this.variableContext = variableContext;
    }

    protected File getArtifactFromTransferTask(final AvailableArtifact artifact)
    {
        // TODO: Don't use null as a valid representation for these ID's.
        final long artifactDownloaderTaskId = Preconditions.checkNotNull(artifact.getArtifactDownloaderTaskId());
        final int artifactDownloaderTransferId = Preconditions.checkNotNull(artifact.getArtifactDownloaderTransferId());

        // Identify the Task definition within this deployment project that did the downloading of this artifact (ie. the transfer task)
        Option<RuntimeTaskDefinition> maybeTaskDefinition = Iterables.findFirst(taskContext.getCommonContext().getRuntimeTaskDefinitions(),
                taskDefinition -> taskDefinition.getId() == artifactDownloaderTaskId);

        if (maybeTaskDefinition.isEmpty())
            return null;

        final RuntimeTaskDefinition artifactDownloaderTaskDefinition = maybeTaskDefinition.get();

        // OK, look up this artifact by it's ID within the transfer task's configuration, and then use this information
        // to locate the physical artifact on the file system.
        final Map<String, String> artDownloaderRuntimeTaskContext = Preconditions.checkNotNull(artifactDownloaderTaskDefinition.getRuntimeContext(), "Artifact Provider Task has no runtime configuration");

        final Collection<Integer> runtimeArtifactIds = ArtifactDownloaderTaskConfigurationHelper.getRuntimeArtifactIds(artDownloaderRuntimeTaskContext, artifactDownloaderTransferId);
        Option<Integer> maybeRuntimeArtifactId = Iterables.findFirst(runtimeArtifactIds, new Predicate<Integer>()
        {
            @Override
            public boolean apply(Integer runtimeArtifactId)
            {
                return artifact.getArtifactId() == ArtifactDownloaderTaskConfigurationHelper.getArtifactId(artDownloaderRuntimeTaskContext, runtimeArtifactId);
            }
        });
        if (maybeRuntimeArtifactId.isEmpty())
            return null;

        // Now locate the actual file.
        final int runtimeArtifactId = maybeRuntimeArtifactId.get();
        String filePattern = ArtifactDownloaderTaskConfigurationHelper.getCopyPattern(artDownloaderRuntimeTaskContext, runtimeArtifactId);
        VariableSubstitutor variableSubstitutor = variableContext.getVariableSubstitutorFactory().newSubstitutorForCommonContext(taskContext.getCommonContext());
        String evaledFilePattern = variableContext.withVariableSubstitutor(variableSubstitutor,
                (Supplier<String>)() -> variableContext.substituteString(filePattern));
        String localPath = ArtifactDownloaderTaskConfigurationHelper.getLocalPath(artDownloaderRuntimeTaskContext, runtimeArtifactId);

        return getFileByAntPath(evaledFilePattern, new File(taskContext.getWorkingDirectory(), localPath));
    }

    protected File getArtifactFromJob(AvailableArtifact artifactDesc)
    {
        TaskContext to = Narrow.to(taskContext, TaskContext.class);
        if (to == null)
        {
            final String msg = "Could not retrieve artifact " + artifactDesc.toString() + " from parent job's artifact subscription since this is a deployment task, which does not understand the concept of artifact subscriptions.";
            log.error(msg);
            // TODO: Return a failure instead of null here?
            return null;

        }

        for (ArtifactSubscriptionContext artifactSubscriptionContext : to.getBuildContext().getArtifactContext().getSubscriptionContexts())
        {
            if (artifactSubscriptionContext.getArtifactDefinitionContext().getId() == artifactDesc.getArtifactId())
            {
                return new File(artifactSubscriptionContext.getEffectiveDestinationPath());
            }
        }

        return null;
    }

    private static File getFileByAntPath(String filePattern, File localPath)
    {
        final List<File> rawArtifacts = new ArrayList<File>();
        final FileVisitor namesVisitor = new FileVisitor(localPath)
        {
            @Override
            public void visitFile(final File file) throws InterruptedException
            {
                rawArtifacts.add(file);
            }
        };

        try
        {
            namesVisitor.visitFilesThatMatch(filePattern);
        }
        catch (InterruptedException e)
        {
            return null;
        }

        return rawArtifacts.isEmpty() ? null : rawArtifacts.get(0);
    }

}
