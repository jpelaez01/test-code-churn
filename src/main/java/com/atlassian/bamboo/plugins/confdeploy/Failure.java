package com.atlassian.bamboo.plugins.confdeploy;

import javax.annotation.Nullable;

/**
 * A failed {@link Result}
 */
public final class Failure extends Result
{
    Failure(String failureMessage, @Nullable Exception exception)
    {
        super(failureMessage, exception);
    }

    @Override
    public boolean isSuccess()
    {
        return false;
    }
}
