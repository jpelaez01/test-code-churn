[#--
    **************** IMPORTANT *****************************
    If you change this template at all, make sure that you
    also update blank-form.html used by the QUnit tests.
    ********************************************************
--]

[#-- @ftlvariable name="action" type="com.atlassian.bamboo.ww2.BambooActionSupport" --]
[#-- @ftlvariable name="context" type="java.util.Map<String, Object>" --]
[#-- @ftlvariable name="product" type="com.atlassian.bamboo.plugins.confdeploy.config.RemoteProductType" --]
[#if stack.findValue("deprecated")?has_content]
<div class="aui-message warning shadowed">
    <p class="title">
        <span class="aui-icon icon-warning"></span>
        <strong>${action.getText("bcpd.config.form.deprecation.title")}</strong>
    </p>
    <p>${action.getText("bcpd.config.form.deprecation.message")}</p>
</div>
[/#if]

[#if stack.findValue("multiProduct")]

    [#-- Target Product. Required! --]
    [@ww.select cssClass="builderSelectWidget long-field" labelKey="bcpd.config.form.product" name="bcpd_config_product" id="bcpd_config_product" list=allProducts listKey="value" listValue="displayName" required=true /]

[#else] [#-- This block can be removed once the deprecated product deploy tasks are removed --]

    [#-- Identifies the target product type to the injected JS--]
    [#-- can't use the webwork macro here because we need to set data- attributes and it doesn't seem to like them. *sigh* --]
    <input type="hidden" id="bcpd_config_product" name="bcpd_config_product" value="${product.getProductKey()}" data-product-name="${action.getText(product.getProductKey())}" data-supports-websudo="${product.getSupportsWebSudo()?string}" />

[/#if]

[#-- Artifact to Deploy. Required! --]
[#assign artifacts=stack.findValue("artifacts") /]
[#if (artifacts ?size) == 0]
    <div class="aui-message warning shadowed">
        <p class="title">
            <span class="aui-icon icon-warning"></span>
            <strong>${action.getText("bcpd.config.form.artifacts.none.title")}</strong>
        </p>
        <p>${action.getText("bcpd.config.form.artifacts.none.html")}</p>
    </div>

    [#-- Make a read-only field to display the existing artifact value, if configured. The user can't change it since there are no artifacts available --]
    [@ww.hidden id="bcpd_config_artifact" name="bcpd_config_artifact" /]
    [@ww.textfield labelKey="bcpd.config.form.artifact" cssClass="long-field" name="bcpd_config_artifact_name" id="bcpd_config_artifact_name" required=true disabled="true" /]
[#else]

    [@ww.select cssClass="builderSelectWidget long-field" labelKey="bcpd.config.form.artifact" id="bcpd_config_artifact" name="bcpd_config_artifact" list=artifacts listKey="value" listValue="displayName" groupBy="group" required=true /]
[/#if]

[#-- Base URL. Required! --]
[@ww.textfield id="bcpd_config_baseUrl" name="bcpd_config_baseUrl" cssClass="long-field" labelKey="bcpd.config.form.baseUrl" required=true /]

<div class="field-group">
    <label for="bcpd_config_credentials">${action.getText('bcpd.config.form.credentials')}<span class="aui-icon icon-required"></span></label>
    [@ww.submit value="${action.getText('bcpd.config.form.credentials.edit')}" class="aui-button" id="bcpd_config_credentials" name="bcpd_config_credentials" /]
    [#if stack.findString("state") == "edit"]
        <div id="stored-credentials-message" class="description" style="display: inline">
            ${action.getText('bcpd.config.form.credentials.msg.some', stack.findString("storedUser"))}
        </div>
    [#else]
        <div id="stored-credentials-message" class="error" style="display: inline;">
            ${action.getText('bcpd.config.form.credentials.msg.none')}
        </div>
    [/#if]
    <div class="description">${action.getText("bcpd.config.form.credentials.description")}</div>
</div>

[#-- Hidden input fields that get updated by the credentials dialog --]
[@ww.hidden id="bcpd_config_username" name="bcpd_config_username" /]
[@ww.hidden id="bcpd_config_usePasswordVariable" name="bcpd_config_usePasswordVariable" /]
[@ww.hidden id="bcpd_config_password" name="bcpd_config_password" /]
[@ww.hidden id="bcpd_config_passwordVariable" name="bcpd_config_passwordVariable" /]
[@ww.hidden id="bcpd_config_useAtlassianId" name="bcpd_config_useAtlassianId" /]
[@ww.hidden id="bcpd_config_atlassianIdBaseURL" name="bcpd_config_atlassianIdBaseURL" /]
[@ww.hidden id="bcpd_config_atlassianIdUsername" name="bcpd_config_atlassianIdUsername" /]
[@ww.hidden id="bcpd_config_atlassianIdPassword" name="bcpd_config_atlassianIdPassword" /]
[@ww.hidden id="bcpd_config_useAtlassianIdPasswordVariable" name="bcpd_config_useAtlassianIdPasswordVariable"  /]
[@ww.hidden id="bcpd_config_atlassianIdPasswordVariable" name="bcpd_config_atlassianIdPasswordVariable" /]
[@ww.hidden id="bcpd_config_useAtlassianIdWebSudo" name="bcpd_config_useAtlassianIdWebSudo" /]
[@ww.hidden id="bcpd_config_atlassianIdAppName" name="bcpd_config_atlassianIdAppName" /]

<h3>${action.getText("bcpd.config.form.advanced")}</h3>

[@ww.checkbox labelKey="bcpd.config.form.branching" name="bcpd_config_branching" /]
[@ww.checkbox labelKey="bcpd.config.form.sslCheck" name="bcpd_config_ssl"  /]
[@ww.checkbox labelKey="bcpd.config.form.logging" name="bcpd_config_logging"  /]
[@ww.textfield labelKey="bcpd.config.form.timeout" name="bcpd_config_timeout" /]

[#-- Write the resource tags required for the edit credentials dialog. This is kind of a hack.. surely there's a better way to do this? --]
${webResources}